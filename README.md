# [ncplug-icfpc2016][]

> Deadline approaches

> Do we have good solution?

> Server responds slowly

``` sh
# Build the project.
stack build

# Run the test suite.
stack test

# Run the benchmarks.
stack bench

# Generate documentation.
stack haddock
```

# Scripts

``` sh
# download all problems using REST API
download-all-problems.sh

# solve problems 1, 2 and 10 using icfpc-solver and submit solutions
solve-problems.sh icfpc-solver 1 2 10
```

# Our team

- Dmitry Vyal aka akamaus
- Alexander Tchitchigin aka gabriel
- Mansur Ziiatdinov aka gltronred
- Sergey Mironov aka grwlf
- Igor Kucheryavenkov aka alar

[ncplug-icfpc2016]: https://github.com/gltronred/ncplug-icfpc2016