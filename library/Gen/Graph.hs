module Gen.Graph
       ( genGraphMain
       ) where

import Gen.Graph.Parser
import Gen.Graph.Worker

import System.Environment
import System.Exit
import Text.Parsec (parse)

-- Launching program

usage :: IO ()
usage = do
  putStrLn "Usage: gen-graph <stub-file> <order> <dx> <dy>"
  putStrLn ""
  putStrLn "<stub-file> contains source points and facets"
  putStrLn "source points will be mapped onto [0,1] x [0,1]"

genGraphMain :: IO ()
genGraphMain = do
  args <- getArgs
  case args of
    [stubFile, order', x', y'] -> do
      estub <- parse stubParser stubFile <$> readFile stubFile
      case estub of
        Left err -> do
          putStrLn $ "Cannot parse " ++ stubFile
          print err
          usage
          exitFailure
        Right stub -> do
          let order = read order'
              x = read x'
              y = read y'
          case generate stub order (x,y) of
            Left err -> print err >> exitFailure
            Right sol -> print sol >> exitSuccess
    _ -> usage >> exitFailure

