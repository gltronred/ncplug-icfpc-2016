module Gen.Graph.Worker
       ( generate
       ) where

import Types hiding (Line)
import qualified Types as T
import PaperFold1 (MPoint(..),Line(..),sourceXY,finalXY,sp,mirrorPoint)
import Gen.Graph.Types
import Gen.Graph.Parser

import Data.List
import qualified Data.Map as M

-- Main solution generation

failIf :: String -> Bool -> Either String ()
failIf msg pred = if pred
                  then Left msg
                  else Right ()

generate :: StubIn -> [(Int,Int)] -> (Rational, Rational) -> Either String Solution
generate stub order dp = do
  scaled <- scale stub
  graph <- connections scaled
  folded <- fold graph scaled order
  moved <- moveTo dp folded
  convert moved

scale :: StubIn -> Either String Stub
scale (points, facets) = let
  xmin = minimum $ map getX points
  xmax = maximum $ map getX points
  ymin = minimum $ map getY points
  ymax = maximum $ map getY points
  dx = unCoord $ xmax - xmin
  dy = unCoord $ ymax - ymin
  in do
    failIf "Ultra thin by X: dx == 0" $ dx == 0
    failIf "Ultra thin by Y: dy == 0" $ dy == 0
    return (map (\(Point x y) -> sp (unCoord x/dx) (unCoord y/dy)) points, facets)

idx :: [a] -> [Int] -> [a]
idx xs is = map (\i -> xs!!i) is

set :: [a] -> [(Int, a)] -> [a]
set xs ys = map (\(i,x) -> case lookup i ys of
                    Nothing -> x
                    Just y -> y) $ zip [0..] xs

center :: [MPoint] -> Point
center pts = let
  n = genericLength pts
  sx = sum $ map psx pts
  sy = sum $ map psy pts
  in Point (Coord $ sx / n) (Coord $ sy / n)

extractEdge :: [MPoint] -> (Vortex, [Int]) -> (Vortex, Line)
extractEdge points (v,[a,b]) = (v, Line (sourceXY $ points!!a) (sourceXY $ points!!b))

extractEdges pts neighbors = do
  failIf "Intersection in >2 points" $ any ((/=2) . length . snd) neighbors
  return $ map (extractEdge pts) neighbors

connections :: Stub -> Either String Graph
connections (points, facets) = do
  let polys = map (center . idx points) facets
      vertices = map Vortex [0..length facets-1]
  edges <- mapM (\facet -> extractEdges points $
                           filter ((>1) . length . snd) $
                           filter ((/=facet) . snd) $
                           zip vertices $
                           map (intersect facet) $
                           facets)
           facets
  failIf "Any facet should bound with another facet" $ any null edges
  return $ M.fromList $ zip vertices edges

fold :: Graph -> Stub -> [(Int,Int)] -> Either String Stub
fold graph stub [] = Right stub
fold graph (points,facets) ((x,y):order) = let
  cur = facets!!x
  neighbors = graph M.! Vortex x
  mneighbor = find ((==Vortex y) . fst) neighbors
  in case mneighbor of
    Nothing -> Left $ "Neighbor not found: " ++ show (x,y) ++ " @ " ++ show graph
    Just (n, line) -> let
      neighbor = facets !! unVortex n
      poly = points `idx` neighbor
      newPoly = map (mirrorPoint line) poly
      in fold graph (points `set` zip neighbor newPoly, facets) order

moveTo :: (Rational,Rational) -> Stub -> Either String Stub
moveTo dp (points, facets) = return (map (movePt dp) points, facets)
  where movePt (dx,dy) pt = pt { px = px pt + dx, py = py pt + dy }

toPoint :: (Rational, Rational) -> T.Point
toPoint (x,y) = T.Point (Coord x) (Coord y)

convert :: Stub -> Either String Solution
convert (points, facets) = do
  failIf "No points" $ length points == 0
  failIf "No facets" $ length facets == 0
  return $ Solution (map (toPoint . sourceXY) points) facets (map (toPoint . finalXY) points)
