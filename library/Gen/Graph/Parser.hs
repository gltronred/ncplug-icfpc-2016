module Gen.Graph.Parser
       ( stubParser
       , parseStub
       ) where

import Types
import Gen.Graph.Types

import Text.Parsec

stubParser :: Parsec String u StubIn
stubParser = (,) <$> (snd <$> sourcesParser) <*> facetsParser

parseStub :: String -> IO (Either ParseError StubIn)
parseStub f = parse stubParser f <$> readFile f

