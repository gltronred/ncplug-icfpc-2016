module Gen.Graph.Types where

import Types hiding (Line)
import PaperFold1 (MPoint,Line)

import Data.Map (Map)

-- Stub is just source points (not necessary to scale) and facets

type StubIn = ([Point], [[Int]])
type Stub = ([MPoint], [[Int]])

newtype Vortex = Vortex { unVortex :: Int } deriving (Eq,Ord,Show)

type Graph = Map Vortex [(Vortex, Line)]

