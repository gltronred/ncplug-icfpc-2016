{-# OPTIONS -fno-warn-missing-methods #-}
{-# LANGUAGE TypeSynonymInstances #-}

-- | Geometric functions concerning vectors.
module Graphics.Gloss.Data.Vector
        ( Vector
        , magV
        , argV
        , dotV
        , detV
        , mulSV
        , rotateV
        , angleVV
        , normalizeV
        , unitVectorAtAngle
        , sqlenV )
where
import Graphics.Gloss.Data.Types
import Graphics.Gloss.Geometry.Angle

-- | The magnitude of a vector.
sqlenV :: (Num n) => Vector n -> n
sqlenV (x, y) = x * x + y * y

-- | The magnitude of a vector.
magV :: (Real n, Floating a) => Vector n -> a
magV (x, y) = sqrt ( fromRational $ toRational $ x * x + y * y)
{-# INLINE magV #-}


-- | The angle of this vector, relative to the +ve x-axis.
argV :: (RealFrac n, RealFloat n, Floating n) => Vector n -> n
argV (x, y)
        = normalizeAngle $ atan2 y x
{-# INLINE argV #-}


-- | The dot product of two vectors.
dotV :: (Num n) => Vector n -> Vector n -> n
dotV (x1, x2) (y1, y2)
        = x1 * y1 + x2 * y2
{-# INLINE dotV #-}


-- | The determinant of two vectors.
detV :: (Num n) => Vector n -> Vector n -> n
detV (x1, y1) (x2, y2)
        = x1 * y2 - y1 * x2
{-# INLINE detV #-}


-- | Multiply a vector by a scalar.
mulSV :: (Num n) => n -> Vector n -> Vector n
mulSV s (x, y)
        = (s * x, s * y)
{-# INLINE mulSV #-}


-- | Rotate a vector by an angle (in radians). +ve angle is counter-clockwise.
rotateV :: (Floating n) => n -> Vector n -> Vector n
rotateV r (x, y)
 =      (  x * cos r - y * sin r
        ,  x * sin r + y * cos r)
{-# INLINE rotateV #-}


-- | Compute the inner angle (in radians) between two vectors.
angleVV :: (Real n, Floating n, Fractional n) => Vector n -> Vector n -> n
angleVV p1 p2
 = let  m1      = magV p1
        m2      = magV p2
        d       = p1 `dotV` p2
        aDiff   = acos $ d / (m1 * m2)

   in   aDiff   
{-# INLINE angleVV #-}


-- | Normalise a vector, so it has a magnitude of 1.
normalizeV :: (Real n, Floating n, Fractional n) => Vector n -> Vector n
normalizeV v    = mulSV (1 / magV v) v
{-# INLINE normalizeV #-}


-- | Produce a unit vector at a given angle relative to the +ve x-axis.
--      The provided angle is in radians.
unitVectorAtAngle :: (Floating n) => n -> Vector n
unitVectorAtAngle r
        = (cos r, sin r)
{-# INLINE unitVectorAtAngle #-}

