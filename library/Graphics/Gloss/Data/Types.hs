{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
module Graphics.Gloss.Data.Types where

-- | A point on the x-y plane.
type Point n     = (n, n)


-- | Pretend a point is a number.
--      Vectors aren't real numbers according to Haskell, because they don't
--      support the multiply and divide field operators. We can pretend they
--      are though, and use the (+) and (-) operators as component-wise
--      addition and subtraction.
--
instance (Num n) => Num (Point n) where
        (+) (x1, y1) (x2, y2)   = (x1 + x2, y1 + y2)
        (-) (x1, y1) (x2, y2)   = (x1 - x2, y1 - y2)
        (*) (x1, y1) (x2, y2)   = (x1 * x2, y1 * y2)
        signum (x, y)           = (signum x, signum y)
        abs    (x, y)           = (abs x, abs y)
        negate (x, y)           = (negate x, negate y)
        fromInteger x           = (fromInteger x, fromInteger x)


-- | A vector can be treated as a point, and vis-versa.
type Vector n     = Point n

plusV :: (Num n) => Vector n -> Vector n -> Vector n
plusV (x1,y1) (x2,y2) = (x1+x2, y1+y2)

minusV :: (Num n) => Vector n -> Vector n -> Vector n
minusV (x1,y1) (x2,y2) = (x1-x2, y1-y2)


-- | A path through the x-y plane.
type Path n     = [Point n]


