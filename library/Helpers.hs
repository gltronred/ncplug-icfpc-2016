{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE LambdaCase #-}
module Helpers where

import Types

import Data.Char (isSpace)
import Data.Ratio
import qualified Data.Vector as V
import Control.Monad.State

-- | Oriented area of polygon (CCW is positive)
area :: Polygon -> Coord
area (Polygon _ vs) = let
  diffs = V.map (minus $ V.head vs) $ V.tail vs
  triangles = V.zipWith cross diffs $ V.tail diffs
  in V.sum triangles * Coord (1 % 2)

shift :: Point -> Polygon -> Polygon
shift (Point x y) polygon = let
  n = Point (-x) (-y)
  in polygon { vertices = V.map (`minus`n) $ vertices polygon }

-- | Центр масс (области выпуклые, поэтому он внутри)
center :: [Point] -> Point
center = (\(Point x y,n) -> Point (x/n) (y/n)) . foldr (\p (s,n) -> (s + p, n+1)) (Point 0 0, 0)

poligon_to_points :: Polygon -> [Point]
poligon_to_points = V.toList . vertices

boundingBox :: Polygon -> Polygon
boundingBox p = let
  verts = vertices p
  xmin = V.minimum $ V.map getX verts
  ymin = V.minimum $ V.map getY verts
  xmax = V.maximum $ V.map getX verts
  ymax = V.maximum $ V.map getY verts
  ps = [Point xmax ymax, Point xmin ymax, Point xmin ymin, Point xmax ymin]
  in Polygon False $ V.fromList ps

boundingPair :: [Point] -> (Point, Point)
boundingPair verts = let
  xmin = minimum $ map getX verts
  ymin = minimum $ map getY verts
  xmax = maximum $ map getX verts
  ymax = maximum $ map getY verts
  in (Point xmin ymin, Point xmax ymax)

boundingBox1 :: [Polygon] -> Polygon
boundingBox1 = foldl1 combine . map boundingBox
  where
    combine p1 p2 = let
      vs1 = vertices p1
      vs2 = vertices p2
      (xmin, ymin) = pmin (vs1 V.! 2) (vs2 V.! 2)
      (xmax, ymax) = pmax (vs1 V.! 0) (vs2 V.! 0)
      ps = [Point xmax ymax, Point xmin ymax, Point xmin ymin, Point xmax ymin]
      in Polygon False $ V.fromList ps

    pmin (Point x1 y1) (Point x2 y2) = (min x1 x2, min y1 y2)
    pmax (Point x1 y1) (Point x2 y2) = (max x1 x2, max y1 y2)


solutionLength :: Solution -> Int -- solution length shouldn't exceed 5000 anyway
solutionLength = length . filter (not . isSpace) . show

solutionFacetSources :: Solution -> [[Point]]
solutionFacetSources (Solution sources facets destinations) =
  map (map (\i -> sources!!i)) facets

solutionFacetDestinations :: Solution -> [[Point]]
solutionFacetDestinations (Solution sources facets destinations) =
  map (map (\i -> destinations!!i)) facets

-- | Calculates an area of either convex or concave polygon
--
-- Source: http://www.mathopenref.com/coordpolygonarea2.html
--
area_concave :: Polygon -> Rational
area_concave Polygon{..} =
  let
    v = V.toList vertices
    xs = map getX v
    ys = map getY v

    indices = [0..length v-1]

    js = last indices : init indices
    is = indices
  in
  flip execState 0 $ do
    forM_ (js`zip`is) $ \((v !!) -> pj, (v !!) -> pi) -> do
      let area = unCoord $ (getX pj + getX pi) * (getY pj - getY pi)
      modify (+area)
    modify (abs.(/2))

point_neighs :: [Point] -> [(Point, Point, Point)]
point_neighs ps = zip3 ps ps2 ps3
  where ps2 = (tail ps ++ [head ps])
        ps3 = (tail ps2 ++ [head ps2])

convex_hull_step :: [Point] -> [Point]
convex_hull_step ps = map (\(p1,p2,p3) -> p2) . filter (\(p1,p2,p3) -> cross (p2-p1) (p3-p2) > 0) $ point_neighs ps

convex_hull ps = stabilize 0 steps
  where steps = iterate convex_hull_step ps
        stabilize k (xs:xxs) | length xs == k = xs
                             | otherwise = stabilize (length xs) xxs
