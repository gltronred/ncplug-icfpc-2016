{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# OPTIONS_GHC -fno-warn-unused-do-bind #-}
{-# OPTIONS_GHC -fno-warn-incomplete-patterns #-}


module Types(Silhouette(..), Skeleton(..), Polygon(..), Point(..), Coord(..), Line(..),
              Problem(..), Solution(..),
              initialSquare, pt,
              minus, cross, isClockwise, fromPoints,
              problemParser, solutionParser, coordParser, pointParser, sourcesParser, facetsParser, destinationsParser,
              P.parse, parseProblem, parseSolution) where

import Control.Monad (guard)
import Data.List
import Data.Maybe
import Data.Ratio
import qualified Data.Vector as V
import Data.Vector (Vector)
import qualified Text.Parsec as P
import qualified Text.Parsec.String as P
import Text.Parsec (Parsec, (<?>))

-- Silhouette

newtype Silhouette = Silhouette
                     { polygons :: Vector Polygon -- набор полигонов
                     } deriving (Eq)

instance Show Silhouette where
  show (Silhouette ps) = show (V.length ps) ++ "\n" ++ concatMap (\x -> show x ++ "\n") ps

silhouetteParser :: Parsec String u Silhouette
silhouetteParser = do
  n <- number <?> "polygons count"
  eoln1
  Silhouette . V.fromList <$> P.count n (polygonParser <* eoln1)

-- Polygon

data Polygon = Polygon
               { clockwise :: Bool -- по часовой - дыра, против - не дыра
               , vertices :: Vector Point -- углы
               } deriving (Eq,Ord)

instance Show Polygon where
  show (Polygon _ v) = show (V.length v) ++ "\n" ++ concatMap (\x -> printPoint x ++ "\n") v

-- | Initial square of paper, not a hole
initialSquare :: Polygon
initialSquare = fromPoints [ Point 0 0, Point 1 0, Point 1 1, Point 0 1 ]

minus :: Point -> Point -> Point
minus (Point a b) (Point c d) = Point (a-c) (b-d)

-- | Cross-product of two vectors
cross :: Point -> Point -> Coord
cross (Point a b) (Point c d) = a*d - b*c

-- | Is set of points CW or CCW
isClockwise :: [Point] -> Bool
isClockwise ps = let
  diffs = map (minus $ head ps) $ tail ps
  triangles = zipWith cross diffs $ tail diffs
  in sum triangles < 0

-- | Polygon from list of points. Assumed that it is a cycle
fromPoints :: [Point] -> Polygon
fromPoints ps = Polygon (isClockwise ps) (V.fromList ps)

polygonParser :: Parsec String u Polygon
polygonParser = do
  n <- number <?> "vertices count"
  eoln1
  ps <- P.count n (pointParser <* eoln1) <?> "vertices of polygon"
  let cw = isClockwise ps
  return $ Polygon cw $ V.fromList ps

-- Point

data Point = Point
             { getX :: Coord
             , getY :: Coord
             } deriving (Show,Eq,Ord)

pt x y = Point (Coord x) (Coord y)

instance Num Point where
  Point ax ay + Point bx by = Point (ax + bx) (ay + by)
  negate (Point ax ay) = Point (negate ax) (negate ay)

printPoint :: Point -> String
printPoint (Point x y) = show x ++ "," ++ show y

pointParser :: Parsec String u Point
pointParser = (\x _ y -> Point x y) <$>
              (coordParser <?> "x coordinate") <*>
              P.char ',' <*>
              (coordParser <?> "y coordinate")

---- Coordinate

newtype Coord = Coord { unCoord :: Rational }
              deriving (Eq,Num,Ord,Fractional)

instance Show Coord where
  show (Coord r) = case denominator r of
    1 -> show (numerator r)
    d -> concat [show (numerator r), "/", show d]

coordParser :: Parsec String u Coord
coordParser = do
  x <- number <?> "denominator"
  my <- P.optionMaybe $ do
    P.char '/'
    number <?> "Fail: numerator"
  return $ Coord $ x % fromMaybe 1 my

-- Skeleton

newtype Skeleton = Skeleton
                   { segments :: Vector Line
                   } deriving (Eq)

instance Show Skeleton where
  show (Skeleton ss) = show (V.length ss) ++ "\n" ++ concatMap (\x -> show x ++ "\n") ss

skeletonParser :: Parsec String u Skeleton
skeletonParser = do
  n <- number <?> "segments count"
  eoln1
  Skeleton . V.fromList <$> P.count n (lineParser <* eoln1)

-- Line

data Line = Line
            { getStart :: Point
            , getEnd :: Point
            } deriving (Eq, Ord)

instance Show Line where
  show (Line s e) = printPoint s ++ " " ++ printPoint e

lineParser :: Parsec String u Line
lineParser = (\x _ y -> Line x y) <$>
             pointParser <*>
             P.char ' '  <*>
             pointParser

-- Problem

data Problem = Problem
               { problemSilhouette :: Silhouette
               , problemSkeleton :: Skeleton
               } deriving (Eq)

instance Show Problem where
  show (Problem si sk) = show si ++ show sk

problemParser :: Parsec String u Problem
problemParser = (\x _ y -> Problem x y) <$>
                silhouetteParser <*>
                eoln1 <*>
                skeletonParser

parseProblem :: String -> IO (Either P.ParseError Problem)
parseProblem = P.parseFromFile problemParser

-- Solution

data Solution = Solution
                { solutionSources :: [Point]
                , solutionFacets :: [[Int]]
                , solutionDestinations :: [Point]
                } deriving (Eq)

instance Show Solution where
  show (Solution sources facets destinations) = show (length sources) ++ "\n" ++
                                                concatMap (\x -> printPoint x ++ "\n") sources ++
                                                show (length facets) ++ "\n" ++
                                                concatMap (\x -> show (length x) ++ " " ++
                                                                 concat (intersperse " " $ map show x) ++
                                                                 "\n") facets ++
                                                concatMap (\x -> printPoint x ++ "\n") destinations

sourcesParser :: Parsec String u (Int, [Point])
sourcesParser = do
  n <- number <?> "sources count"
  eoln1
  ss <- P.count n (pointParser <* P.optional eoln1) <?> "sources"
  guard $ n == length ss
  eoln1 <?> "end sources"
  return (n,ss)

facetsParser :: Parsec String u [[Int]]
facetsParser = do
  m <- number <?> "facets count"
  eoln1
  fs <- P.count m $ do
    r <- number <?> "facet vertices count"
    -- eoln1
    P.char ' '
    x <- P.count r $ number <* P.optional (P.char ' ')
    guard $ r == length x
    P.try eoln1
    return x
  guard $ m == length fs
  eoln1 <?> "end facets"
  return fs

destinationsParser :: Int -> Parsec String u [Point]
destinationsParser n = do
  ds <- P.count n (pointParser <* P.optional eoln1) <?> "destinations"
  eoln1 <?> "end destinations"
  guard $ n == length ds
  return ds

solutionParser :: Parsec String u Solution
solutionParser = do
  (n,ss) <- sourcesParser
  fs <- facetsParser
  ds <- destinationsParser n
  return $ Solution ss fs ds

parseSolution :: String -> Either P.ParseError Solution
parseSolution = P.parse solutionParser "String"

-- helper functions
number :: (Read a, Integral a) => Parsec String u a
number = do
  ms <- P.optionMaybe $ P.char '-'
  x <- P.many1 P.digit
  return $ case ms of
    Nothing -> read x
    Just '-' -> -read x

eoln :: Parsec String u ()
eoln = P.char '\n' *> pure ()

eoln1 :: Parsec String u [()]
eoln1 = P.many eoln
