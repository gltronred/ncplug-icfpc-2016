module Solution.S1392
       ( solve1392Main
       ) where

import Helpers
import Huzita
import Huzita.Interpreter1
import Types
import Solution.Wrapper

import Data.Function
import Data.List
import Data.Ratio
import qualified Data.Vector as V

import Debug.Trace

isSquare (Point x y) = let
  s = unCoord $ x^2 + y^2
  p, q :: Integer
  p = numerator s
  q = denominator s
  sqrtRounded = truncate . (sqrt :: Double -> Double) . fromIntegral
  self = (^2) . sqrtRounded
  in self p == p && self q == q

goodLine (Line p1 p2) = isSquare $ p2 - p1

getGoodLines :: Problem -> [(Point, Point)]
getGoodLines problem = let
  points = concatMap poligon_to_points $ V.toList $ polygons $ problemSilhouette problem
  lines = zipWith Line points (tail points ++ [head points])
  in map (\(Line p1 p2) -> (p2-p1, p1)) $ filter goodLine lines

len (Point x y) = unCoord $ x^2 + y^2
dist p1 p2 = len $ p1-p2

solve1392 :: Problem -> Huzita ()
solve1392 problem = do
  let (Point dx dy,p1) = maximumBy (\(d1,_)(d2,_) -> compare (len d1) (len d2) ) $ getGoodLines problem
      rot = Point dx dy
      axis = getAxis $ transform rot (pt 0 0)
      prob = itransformProblem axis problem
      sil = V.head $ polygons $ problemSilhouette prob
      verts = convex_hull $ poligon_to_points sil
      (p_min, p_max) = boundingPair verts
      [pmin,pmax] = map (switchAxis axis) [p_min,p_max]
      bc = center [pmin, pmax]
  transform rot $ pmin


solve1392Main :: Problem -> Huzita ()
solve1392Main p = solve1392 p >> solverWrap p
