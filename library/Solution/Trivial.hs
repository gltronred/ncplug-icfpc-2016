{-# OPTIONS_GHC -fno-warn-incomplete-patterns #-}

module Solution.Trivial
       ( trivial
       ) where

import Types hiding (Point, Line)
import qualified Types as T
import Helpers
import PaperFold1

import Data.List (find)
-- import Data.Ratio
import qualified Data.Vector as V

foldOver :: [Line] -> MPoint -> MPoint
foldOver ls p = foldr mirrorPoint p ls

toPoint :: (Rational, Rational) -> T.Point
toPoint (a,b) = T.Point (Coord a) (Coord b)

solutionForRectangle :: Polygon -> Solution
solutionForRectangle poly = let
  [p1,p2,p3,p4] = V.toList $ vertices poly
  Just a = find (/= 0) $ map (\p -> getX p - getX p1) [p2,p3,p4]
  Just b = find (/= 0) $ map (\p -> getY p - getY p1) [p2,p3,p4]
  na' = 1 / a
  nb' = 1 / b
  na :: Int -- FIXME: we shouldn't ever encounter big integers here, should we?
  na = truncate $ unCoord na'
  nb = truncate $ unCoord nb'
  lastA = 1 - fromIntegral na*a
  lastB = 1 - fromIntegral nb*b

  naCorr = if lastA>0 then na+1 else na
  nbCorr = if lastB>0 then nb+1 else nb

  fromI i = unCoord $ if i > na then 1 else fromIntegral i * a
  fromJ j = unCoord $ if j > nb then 1 else fromIntegral j * b

  vlines i = map (\k -> Line (fromI k, 0) (fromI k, 1)) [1 .. i-1]
  hlines j = map (\k -> Line (0, fromJ k) (1, fromJ k)) [1 .. j-1]
  lines i j = vlines i ++ hlines j

  points = map (\(i,j) -> foldOver (lines i j) $ sp (fromI i) (fromJ j))
           [ (i,j) | i <- [0 .. naCorr ]
                   , j <- [0 .. nbCorr ] ]

  sources = map (toPoint . sourceXY) points
  facets = map (\(i,j) -> map fromIntegral [ i*(nbCorr+1)+j
                                           , i*(nbCorr+1)+j+1
                                           , (i+1)*(nbCorr+1)+j+1
                                           , (i+1)*(nbCorr+1)+j])
           [(i,j) | i<-[0..naCorr-1], j<-[0..nbCorr-1]]
  destinations = map (toPoint . finalXY) points
  in Solution { solutionSources = sources
                , solutionFacets = facets
                , solutionDestinations = destinations
                }

trivial :: Problem -> Solution
trivial problem = let
  polys@(poly : _) = V.toList $ polygons $ problemSilhouette problem
  in if (V.length . vertices) poly == 4
     then let
      [p1,p2,p3,p4] = V.toList $ vertices poly
      a:as = filter (/= 0) $ map (\p -> getX p - getX p1) [p2,p3,p4]
      b:bs = filter (/= 0) $ map (\p -> getY p - getY p1) [p2,p3,p4]
      in if all (== a) as && all (== b) bs -- it is a rect
         then solutionForRectangle poly
         else --error "Cannot solve this case!"
              solutionForRectangle $ boundingBox poly
     else solutionForRectangle $ boundingBox1 polys


{-
let [p1,p2,p3,p4] = V.toList $ vertices $ V.head $ polygons $ problemSilhouette p
let Just a = find (/= 0) $ map (\p -> getX p - getX p1) [p2,p3,p4]
let Just b = find (/= 0) $ map (\p -> getY p - getY p1) [p2,p3,p4]
let na = truncate $ unCoord $ 1/a :: Int
let nb = truncate $ unCoord $ 1/b :: Int
let lastA = 1 - fromIntegral na*a
let lastB = 1 - fromIntegral nb*b
let naCorr = if lastA>0 then na+1 else na
let nbCorr = if lastB>0 then nb+1 else nb
let fromI i = unCoord $ if i > na then 1 else fromIntegral i * a
let fromJ j = unCoord $ if j > nb then 1 else fromIntegral j * b
-}

