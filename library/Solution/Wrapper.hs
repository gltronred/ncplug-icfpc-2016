module Solution.Wrapper where

import Helpers
import Huzita
import Types

import qualified Data.Vector as V
import Control.Monad (when)

solverWrap :: Problem -> Huzita ()
solverWrap prob = let
  sil = V.head $ polygons $ problemSilhouette prob
  verts = convex_hull $ poligon_to_points sil

  edges = zipWith Line verts (tail verts ++ [head verts])

  fold_while_folds = do
    res <- mapM fold edges
    when (or res) fold_while_folds

  in do
    fold_while_folds
    end
