module Huzita.Interpreter0
       ( runHuzitaPure
       , OrigamiTransformation(..)
       , initial
       ) where

import Types
import Huzita.Monad

import Control.Monad.Free
import qualified Data.Map.Strict as M
import Data.Map.Strict (Map)
import qualified Data.Vector as V

-- | State of Origami
data OrigamiTransformation =
  OrigamiTransformation
  { points :: Map String Point -- | Points set in outer coordinates
  , lines :: Map String Line -- | Lines set in outer coordinates
  , folds :: [Line]  -- | List of folds
  } deriving (Eq,Show)

-- | Initially we start with unit square
initial :: OrigamiTransformation
initial = OrigamiTransformation points lines []
  where lines = M.fromList
                [ ("00-10", Line p1 p2)
                , ("10-11", Line p2 p3)
                , ("11-01", Line p3 p4)
                , ("01-00", Line p4 p1)
                ]

        [p1,p2,p3,p4] = [ Point (Coord 0) (Coord 0)
                        , Point (Coord 1) (Coord 0)
                        , Point (Coord 1) (Coord 1)
                        , Point (Coord 0) (Coord 1)
                        ]
        points = M.fromList [("00",p1), ("10",p2), ("11",p3), ("01",p4)]

---------- Pure interpreter

runHuzitaPure :: Huzita a -> OrigamiTransformation -> OrigamiTransformation
runHuzitaPure (Pure r) st = st
runHuzitaPure (Free (Transform shift rotate next)) st = runHuzitaPure next $ shiftRotate shift rotate st
runHuzitaPure (Free (Fold l next)) st = runHuzitaPure (next True) $ performFold l st
runHuzitaPure (Free End) st = st

shiftRotate :: Point -> Point -> OrigamiTransformation -> OrigamiTransformation
shiftRotate shift rotate state = undefined

performFold :: Line -> OrigamiTransformation -> OrigamiTransformation
performFold l state = state { folds = l : folds state }

