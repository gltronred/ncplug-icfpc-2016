module Huzita.Interpreter1
       ( solveHuzita1
       , runHuzita1
       , Transformation (..)
       , Origami()
       , initialState
       , toSolution
       -- for tests
       , Matrix
       , axisE
       , toParamLine
       , mult
       , apply
       , eye
       , sign
       , reflectAcross
       , switchAxis
       , switchAxis'
       , inverseAxis
       , runHuzitaDebug
       , reflMatrix
       , ParamLine
       , toParamLine
       , intersectPoly
       , toCCW
       , squareRoot
       , getNewFacets
       , center
       , Origami(..)
       , itransformPoly
       , itransformProblem
       , getAxis
       ) where

import Types
import Huzita.Monad
import Helpers

import Control.Monad.Free
import Data.Function
import Data.List
import qualified Data.Map.Strict as M
import Data.Map.Strict (Map)
import Data.Maybe
import Data.Ratio
import qualified Data.Vector as V
import qualified Graphics.Gloss.Data.Types as Gloss
import Graphics.Gloss.Geometry.Line


type Matrix = ((Coord, Coord), (Coord, Coord))

mult :: Matrix -> Matrix -> Matrix
mult ((a11,a12),(a21,a22)) ((b11,b12),(b21,b22)) =
  ((a11*b11+a12*b21, a11*b12+a12*b22),
   (a21*b11+a22*b21, a21*b12+a22*b22))

--det :: Matrix -> Rational
det ((a11,a12),(a21,a22)) = a11 * a22 - a12 * a21

apply :: Matrix -> Point -> Point
apply ((a11,a12),(a21,a22)) (Point b1 b2) = Point (a11*b1+a12*b2) (a21*b1+a22*b2)

eye :: Matrix
eye = ((1,0),
       (0,1))

axisE = (eye, pt 0 0)

type ParamLine = (Coord, Coord, Coord)

toParamLine :: Line -> ParamLine
toParamLine (Line p1 p2) = (-dy, dx, c)
  where dx = getX p2 - getX p1
        dy = getY p2 - getY p1
        c = - dx * getY p1 + dy * getX p1

-- | Transformations from outer coordinate system to inner
type Axis = (Matrix, Point)

-- | State of Origami
data Origami = Origami
               { folds :: [Line] -- ^ Folds set in outer coordinates
               , lines :: [Line] -- ^ All lines inside unit square
               , areas :: [([Point], Axis)]  -- ^ Several sheets, each with own (inner) coordinate system
               } deriving (Eq,Show)

type Transformation = Origami -> Origami

-- | Initially we start with unit square
initialState :: Origami
initialState = Origami [] lines [(square, (eye,Point 0 0))]
  where lines = [ Line p1 p2
                , Line p2 p3
                , Line p3 p4
                , Line p4 p1
                ]

        square@[p1,p2,p3,p4] = [pt 0 0, pt 1 0, pt 1 1, pt 0 1]

-- | Convert current state to solution
toSolution :: Origami -> Solution
toSolution st = Solution srcs facets dests
  where point_pairs = nubBy (\a b -> fst a == fst b) $ concatMap (\(ps, tr) -> map (\p -> (p, switchAxis tr p)) ps) $ areas st
        srcs = map fst point_pairs
        dests = map snd point_pairs
        sortCCW fct = sortBy (compare `on` angle . flip minus (center fct)) fct
        facets = [ map (\x -> fromJust $ x `elemIndex` srcs) $ sortCCW $ fst facet | facet <- areas st ]

---------- Interpreter

runHuzita1 :: Huzita () -> Transformation
runHuzita1 (Pure r) st = st
runHuzita1 (Free (Transform rotate shift next)) st = runHuzita1 next $ shiftRotate rotate shift st
runHuzita1 (Free (Fold l next)) st = case performFold l st of
                                       Just st' -> runHuzita1 (next True) $ st'
                                       Nothing -> runHuzita1 (next False) $ st
runHuzita1 (Free (GetSolution next)) st = let
  sol = toSolution st
  in runHuzita1 (next sol) st
runHuzita1 (Free End) st = st

solveHuzita1 :: Huzita () -> Solution
solveHuzita1 m = toSolution $ runHuzita1 m initialState

-- | С какой стороны от линии находится точка
sign :: ParamLine -> Point -> Ordering
sign (a,b,c) (Point x y) = compare (a*x + b*y + c) 0

-- | Матрица отражения
reflMatrix :: ParamLine -> Matrix
reflMatrix (a,b,c) = ( ((a^2-b^2)/d, 2*a*b/d),
                       (2*a*b/d, (b^2-a^2)/d) )
  where d = negate $ a^2 + b^2

(^!) :: Num a => a -> Int -> a
(^!) x n = x^n

squareRoot :: Integer -> Integer
squareRoot 0 = 0
squareRoot 1 = 1
squareRoot n =
   let twopows = iterate (^!2) 2
       (lowerRoot, lowerN) =
          last $ takeWhile ((n>=) . snd) $ (0,1) : zip (1:twopows) twopows
       newtonStep x = (x + n `div` x) `div` 2
       iters = iterate newtonStep (squareRoot (n `div` lowerN) * lowerRoot)
       isRoot r  =  r*r == n
  in  head $ dropWhile (not . isRoot) iters

-- | Сдвиг и поворот
-- shift - указывает направление, куда сдвинется вектор (1,0)
-- shift - не обязательно единичной длины. TODO: Как правильно поворачивать для рациональных чисел
shiftRotate :: Point -> Point -> Origami -> Origami
shiftRotate rotate@(Point x y) shift state = let
  r2 = unCoord $ x^2 + y^2
  p = numerator r2
  q = denominator r2
  r = Coord $ squareRoot p % squareRoot q
  a = x / r
  b = y / r
  axis = (((b,a),
           (-a,b)),
          shift)
  in state { areas = map (\(ps,a) -> (ps, composeTrans axis a)) $ areas state }

-- | Каждый сгиб делит старую область на одну или две части
getNewFacets :: Line -> ([Point], Axis) -> [([Point], Axis)]
getNewFacets line (poly, axis) = let
  l = switchAxis' (inverseAxis axis) line
  -- Менять в области преобразование координат?
  changeAxis p = case sign (toParamLine l) (center p) of
                   LT -> (p, reflectAcross line axis)
                   GT -> (p, axis)
                   EQ -> error $ "flat shape" ++ show p
  in map changeAxis $ filter ((>2) . length) $ case intersectPoly l poly of
    (p1, Nothing) -> [p1]
    (p1, Just p2) -> [p1, p2]

-- | Выполнение сгиба
performFold :: Line -> Origami -> Maybe Origami
performFold l state = if length oldFacets < length newFacets then
  Just $ state { folds = l : folds state
               , areas = newFacets
               }
  else Nothing
  where
  oldFacets = areas state
  newFacets = concatMap (getNewFacets l) oldFacets

-- | Преобразование в формат Gloss
toGloss :: Point -> Gloss.Point Coord
toGloss (Point a b) = (a,b)

angle :: Point -> Double
angle (Point x y) = atan2 (toDouble y) (toDouble x)
  where toDouble = fromRational . unCoord

toCCW :: [Point] -> [Point]
toCCW poly = let
  c = center poly
  in sortBy (compare `on` angle . flip minus c) poly

-- | Пересечение выпуклого полигона линией
intersectPoly :: Line -> [Point] -> ([Point], Maybe [Point])
intersectPoly l@(Line p3 p4) poly' = let
  poly = toCCW poly'
  lines = zipWith Line poly $ last poly : init poly
  inters = mapMaybe (\(Line p1 p2) -> uncurry Point <$>
                                      intersectSegLine (toGloss p1) (toGloss p2) (toGloss p3) (toGloss p4)) lines
  in case nub inters of
    [] -> (poly, Nothing)
    [x] -> (poly, Nothing) -- касание, ничего не меняем
    [x,y] -> let poly1 = y : x : filter (\p -> sign (toParamLine l) p == GT) poly
                 poly2 = x : y : filter (\p -> sign (toParamLine l) p == LT) poly
             in case (length poly1, length poly2) of
               (2,2) -> error "Got two lines o_O"
               (2,_) -> (poly2, Nothing)
               (_,2) -> (poly1, Nothing)
               (x,y) | x>2 && y>2 -> (poly1, Just poly2)
                     | otherwise -> error "Got single point o_O"
    xs -> error $ "Something went wrong: " ++ show poly ++ " - " ++ show l ++ ": " ++ show inters

neg :: Point -> Point
neg = negate

-- | Переход в новую систему координат для точки
switchAxis :: Axis -> Point -> Point
switchAxis (a,b) p = let
  in apply a p + b

-- | Переход в новую систему координат для линии
switchAxis' :: Axis -> Line -> Line
switchAxis' axis (Line p1 p2) | det (fst axis) >= 0 = Line (switchAxis axis p1) (switchAxis axis p2)
                              | otherwise = Line (switchAxis axis p2) (switchAxis axis p1)

-- | Обратное преобразование
-- A^{-1}(Ax+b) - A^{-1}b = x
inverseAxis :: Axis -> Axis
inverseAxis (((a,b),(c,d)), (Point x y)) = let
  det = a*d-b*c
  inv = ((d/det, -b/det), (-c/det, a/det))
  in (inv, apply inv (Point (-x) (-y)))

lineReflection :: Line -> Axis
lineReflection l@(Line p1 p2) = let
  ref_m = reflMatrix $ toParamLine l
  in (ref_m, p1 - apply ref_m p1)

composeTrans :: Axis -> Axis -> Axis
composeTrans (a2,b2) (a1,b1) = (mult a2 a1, apply a2 b1 + b2)

-- | Отражение относительно линии
-- Сдвигаем начало координат к (преобразованному) началу линии и умножаем на матрицу отражения
-- M(Ax+b-p1) = (MA)x + M(b-p1)
reflectAcross :: Line -> Axis -> Axis
reflectAcross l@(Line p1 p2) ax@(a,b) = composeTrans (lineReflection l) ax

runHuzitaDebug :: Huzita () -> Origami -> IO Origami
runHuzitaDebug (Pure r) st = do print $ "Pure" ++ show st
                                return st
runHuzitaDebug (Free (Transform shift rotate next)) st = do
  let st' = shiftRotate shift rotate st
  print "Transform: "
  print st'
  runHuzitaDebug next st'
runHuzitaDebug (Free (Fold l next)) st = do
  let mst' = performFold l st
  print $ "Fold: " ++ show mst'
  case mst' of
    Just st' -> runHuzitaDebug (next True) st'
    Nothing -> runHuzitaDebug (next False) st

runHuzitaDebug (Free (GetSolution next)) st = do
  let sol = toSolution st
  print "getSolution: "
  runHuzitaDebug (next sol) st
runHuzitaDebug (Free End) st = do print $ "End: " ++ show st
                                  return st



-- Prepare inverse transformation to problem (useful with transforming solution)
itransformPoly :: (Matrix,Point) -> Polygon -> Polygon
itransformPoly axis (Polygon f ps) = Polygon f $ V.map (switchAxis axis) ps

itransformProblem :: Axis -> Problem -> Problem
itransformProblem axis p = let
  inv = inverseAxis axis
  polys = polygons $ problemSilhouette p
  newPolys = Silhouette $ V.map (itransformPoly inv) polys
  newSkel = Skeleton $ V.map (switchAxis' inv) $ segments $ problemSkeleton p
  in Problem newPolys newSkel

getAxis h = snd $ head $ areas $ runHuzita1 h initialState

