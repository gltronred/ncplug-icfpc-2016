module Huzita.Monad
       ( Huzita()
       , HuzitaF(..)
       , getSolution
       , transform
       , fold
       , getSolution
       , end
       ) where

import Prelude
import Types

import Control.Monad.Free
import Data.Functor

---------- Free Monad for transformations

data HuzitaF next = Transform Point Point next -- ^ Куда сдвигается начало координат и куда переходит (0,r) [2]
                  | Fold Line (Bool -> next) -- ^ Поворот вокруг линии; часть, которая находится слева, остаётся [1]
                  | GetSolution (Solution -> next) -- ^ Получить текущее решение
                  | End            -- ^ Конец

{- [1] Часть, которая находится слева, остаётся

Линия задана точками 1 и 2

+-2-+      +-2
|  \|      | |\
0   1  --> | +-1
|   |      |   |
+---+      +---+

-}

{- [2] Куда сдвигается начало координат и куда переходит (0,r)

Вначале выполняется поворот, а затем сдвиг

Transform (3,4) (1,2) означает:
- повернуть координаты так, чтобы (0,5) перешло в (3,4)
- сдвинуть начало координат в (1,2)

-}

instance Functor HuzitaF where
  fmap f (Transform rotate shift next) = Transform rotate shift $ f next
  fmap f (Fold l next) = Fold l $ f . next
  fmap f (GetSolution next) = GetSolution $ f . next
  fmap f End = End

type Huzita = Free HuzitaF

transform :: Point -> Point -> Huzita ()
transform rotate shift = liftF $ Transform rotate shift ()

fold :: Line -> Huzita Bool
fold by = liftF $ Fold by id

getSolution :: Huzita Solution
getSolution = liftF $ GetSolution id

end :: Huzita ()
end = liftF End

