module Huzita.Axioms
       ( axiom0
       , axiom1
       , axiom2
       , axiom3
       , axiom4
       , axiom5
       , axiom6
       , axiom7
       ) where

import Types
import Huzita.Monad

perp :: Line -> Line
perp (Line p1 p2) = let
  (dx,dy) = (getX p2 - getX p1, getY p2 - getY p1)
  in Line p1 $ Point (getX p1 + dy) (getY p1 - dx)

-- | Rotate and shift
axiom0 :: Coord -> Point -> Huzita ()
axiom0 rotate shift = undefined

-- | Axiom 1. Given two points, there is unique fold passing through both of them
axiom1 :: Point -> Point -> Huzita Bool
axiom1 p1 p2 = fold (Line p1 p2)

-- | Axiom 2. Given two points p1 and p2, there is a unique fold that places p1 onto p2.
axiom2 :: Point -> Point -> Huzita ()
axiom2 p1 p2 = undefined

-- | Axiom 3. Given two lines l1 and l2, there is a fold that places l1 onto l2.
axiom3 :: Line -> Line -> Huzita ()
axiom3 l1 l2 = undefined

-- | Axiom 4. Given a point p1 and a line l1, there is a unique fold perpendicular to l1 that passes through point p1.
axiom4 :: Point -> Line -> Huzita ()
axiom4 p1 l1 = undefined

-- | Axiom 5. Given two points p1 and p2 and a line l1, there is a fold that places p1 onto l1 and passes through p2.
axiom5 :: Point -> Point -> Line -> Huzita ()
axiom5 p1 p2 l1 = undefined

-- | Axiom 6. Given two points p1 and p2 and two lines l1 and l2, there is a fold that places p1 onto l1 and p2 onto l2.
axiom6 :: Point -> Point -> Line -> Line -> Huzita ()
axiom6 p1 p2 l1 l2 = undefined

-- | Axiom 7. Given one point p and two lines l1 and l2, there is a fold that places p onto l1 and is perpendicular to l2.
axiom7 :: Point -> Line -> Line -> Huzita ()
axiom7 p l1 l2 = undefined

