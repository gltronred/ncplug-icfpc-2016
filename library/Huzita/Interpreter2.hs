{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ViewPatterns #-}
module Huzita.Interpreter2 where

import Data.Map(Map)
import qualified Data.Map as Map
import Data.Vector(Vector)
import qualified Data.Vector as Vector
import PaperFold1 (MPoint(..),Paper(..))
import qualified PaperFold1 as PF
import Control.Monad.Free
import Data.Monoid
import Debug.Trace

import Types
import Huzita.Monad
import qualified Huzita.Interpreter1 as I1


-- | State of Origami
data Origami = Origami {
     papers :: [Paper]  -- ^ Several sheets
   }deriving (Show)

type Transformation = Origami -> Origami

initialOrigami :: Origami
initialOrigami = Origami [PF.initialPaper]

-- end :: Huzita ()
-- end = Pure ()

runHuzita2 :: Huzita a -> Transformation
runHuzita2 (Pure r) st = st
runHuzita2 (Free (Transform rotate shift next)) st = runHuzita2 next $ performShift shift st
runHuzita2 (Free (Fold line next)) st = case performFold line st of
  Just st' -> runHuzita2 (next True) $ st'
  Nothing -> runHuzita2 (next False) $ st
runHuzita2 (Free End) st = st

performShift :: Point -> Origami -> Origami
performShift shift_p (Origami pp) = Origami $ map (\(Paper paper) -> Paper $ map (PF.shiftMPoint `flip` shift_p) paper) pp

solveHuzita2 :: Huzita a -> Solution
solveHuzita2 m = PF.toSolution $ papers $ (runHuzita2 m) initialOrigami

pfpoint  :: Point -> (Rational, Rational)
pfpoint Point{..} = (unCoord getX,unCoord getY)

hpoint :: PF.MPoint -> Point
hpoint PF.MPoint{..} = Point (Coord px) (Coord py)

pfline :: Line -> PF.Line
pfline Line{..} = PF.Line (pfpoint getStart) (pfpoint getEnd)

pointSign :: Line -> Point -> Ordering
pointSign (I1.toParamLine -> (a,b,c)) (Point x y) = (a*x + b*y + c) `compare` 0

paperSign :: Line -> Paper -> Ordering
paperSign l p@(Paper{..}) = foldl go EQ $ map (pointSign l . hpoint) paper_vertices where
  go EQ x = x
  go x EQ = x
  go x y
    | x == y = x
    | otherwise = error $ "psign: line " <> show l <> " crosses paper " <> show p

-- Получаем точку слева от линии
getLeftPoint :: Line -> Point
getLeftPoint (Line p1 p2) =
  p1 - rotateLeft (p2 - p1)

-- Поворот вектора на 90
rotateLeft :: Point -> Point
rotateLeft (Point (Coord x) (Coord y)) =
  let
    (x',y') = (-y,x)
  in
  Point (Coord $ toRational x') (Coord $ toRational y')

performFold :: Line -> Origami -> Maybe Origami
performFold l'@(pfline -> l) Origami{..} = let
  papers' = flip concatMap papers $ \p@Paper{..} ->
    case PF.cutPaper p l of
      [p1,p2] -> [p1, PF.mirrorPaper p2 l]
      [p] -> [p]

  in if length papers < length papers'
     then Just $ Origami papers'
     else Nothing

--- Test

task1 :: Huzita Bool
task1 =
  let
    p1 = pt 1 0
    p2 = pt 0 1
    l1 = Line p1 p2

    p3 = pt 0 0.5
    p4 = pt 1 0.5
    l2 = Line p3 p4

  in do

  fold l1 -- (pt 1 1)
  fold l2 -- (pt 1 1)


s1 = solveHuzita2 task1


