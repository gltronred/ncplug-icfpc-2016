{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE NondecreasingIndentation #-}
{-# LANGUAGE FlexibleContexts #-}

module PaperFold1 where

import Debug.Trace
import Data.Maybe
import Data.Ratio
import Control.Monad.State.Strict
import Control.Monad.Writer
import Data.Monoid
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Vector(Vector)
import qualified Data.Vector as Vector
import Data.Foldable
import Control.Arrow ((&&&),(***))
import Text.Show.Pretty
import Data.List (nub, elemIndex, findIndex, (!!))
import Data.Function (on)
import Control.Lens (Lens', Lens, makeLenses, (%=), (^.), set, view, use, _1, _2, _3, _4, _5, _6)
import Control.Monad.Cont
import Text.Printf

import Types hiding (Line,Point)
import qualified Types as Types
import Gloss hiding (Point)
import qualified Gloss as Gloss



-- Monadic showable version of trace
traceMs :: (Monad m, Show a) => a -> m ()
traceMs a = trace (ppShow a) (return ())

trace' :: (Show a) => a -> b -> b
trace' a b = trace (ppShow a) b

-- | Point wich memorises its initial source position
data MPoint = MPoint {
      px :: Rational -- | Final (latest) X position
    , py :: Rational -- | Final (latest) Y position
    , psx :: Rational -- | Source X position
    , psy :: Rational -- | Source Y position
  } deriving(Eq,Ord,Show)

-- | sourcePoint: point in the initial coordinate system
sp :: Rational -> Rational -> MPoint
sp x y = MPoint x y x y

-- | Get final coords
finalXY :: MPoint -> (Rational, Rational)
finalXY MPoint{..} = (px, py)

-- | Get source coord
sourceXY :: MPoint -> (Rational, Rational)
sourceXY MPoint{..} = (psx, psy)

-- | Takes 'memory' segment (@p1@,@p2@), one 'final' point @p@ (aka Gloss
-- point) which belongs to the segment [p1,p2]. Returns the 'memory' point
-- corresponding to @p@
sourceSeg :: MPoint -> MPoint -> (Rational, Rational) -> MPoint
sourceSeg p1 p2 p@(fx, fy) = MPoint fx fy sx sy where
  (fx1, fy1) = finalXY p1
  (fx2, fy2) = finalXY p2
  (sx1, sy1) = sourceXY p1
  (sx2, sy2) = sourceXY p2

  {- proportion in final coordinates -}
  c = if fx2 /= fx1 then (fx-fx1)/(fx2-fx1) else
        if fy2 /= fy1 then (fy-fy1)/(fy2-fy1) else 0

  {- source coordinates should have same proportion -}
  sx = c * (sx2-sx1) + sx1
  sy = c * (sy2-sy1) + sy1

-- | Apply final transformation
moveMPoint :: MPoint -> Gloss.Point Rational -> MPoint
moveMPoint p (x,y) = p { px = x, py = y }

shiftMPoint :: MPoint -> Types.Point -> MPoint
shiftMPoint p (Types.Point x y) = p { px = px p + unCoord x , py = py p + unCoord y }

-- Sheet of paper. (==) should be called on normalized papers only
data Paper = Paper { paper_vertices :: [MPoint] }
  deriving (Show,Eq)

shift1 :: [a] -> [a]
shift1 (h:hs) = hs <> [h]
shift1 [] = []

shifts :: [a] -> [[a]]
shifts l = fst $ foldl' (\(res,l) i -> let l' = shift1 l in (l' : res, l')) ([],l) [0..(length l-1)]

printMPoint :: MPoint -> String
printMPoint = (\(finalXY -> (x,y)) -> printf "(%.3f, %.3f)" (r2f x) (r2f y)) where
  r2f :: Rational -> Double
  r2f = fromRational . toRational

printPaper :: Paper -> String
printPaper p = concatMap printMPoint $ paper_vertices p

-- | Infinite line
data Line = Line { lp1 :: (Rational,Rational), lp2 :: (Rational, Rational) }
  deriving(Show)

initialPaper = Paper [ sp 0 0, sp 1 0, sp 1 1, sp 0 1 ]


-- | Make the first point of paper closest to (0,0)
normalizePaper :: Paper -> Paper
normalizePaper Paper{..} =
  Paper $ minimumBy (compare `on` sqlenV . finalXY . head) $ shifts paper_vertices

-- | Side of a line fo point
--
-- GT - to the right
-- LT - to the left
-- EQ - on the line
pointSide :: Line -> MPoint -> Ordering
pointSide (Line (x1,y1) (x2,y2)) MPoint{..} =
  ((px-x1)*(y2-y1) - (py-y1)*(x2-x1)) `compare` 0

-- | Side of a line for segment
segSide :: Line -> MPoint -> MPoint -> Ordering
segSide l p1 p2 =
  case (pointSide l p1, pointSide l p2) of
    (EQ,x) -> x
    (x,EQ) -> x
    (x,y) | x == y -> x
          | otherwise -> error "segSide: segment crosses the line"

cutPaper :: Paper -> Line -> [Paper]
cutPaper Paper{..} line@(Line p1 p2) =
  let
    v = paper_vertices
    edjes = (v `zip` ((tail v) <> [head v]))

    split (s1,sign1,s2,sign2) = do
      case intersectSegLine (finalXY s1) (finalXY s2) p1 p2 of
        Just (sourceSeg s1 s2 -> pi) -> do
          tell [(s1,sign1)] >> tell [(pi,EQ)] >> tell [(s2,sign2)]
        Nothing -> do
          error $ "Segment " <> show s1 <> "-" <> show s2 <> " does not actually crosses the line " <> show line

    points =
      nub $ execWriter $ do
        forM_ edjes $ \(s1,s2) -> do
          case (pointSide line s1, pointSide line s2) of
            (LT,GT) -> split (s1,LT,s2,GT)
            (GT,LT) -> split (s1,GT,s2,LT)
            (x,y) -> tell [(s1,x)] >> tell [(s2,y)]

    pl = map fst $ filter (\(p,sign) -> sign == LT || sign == EQ) points
    pr = map fst $ filter (\(p,sign) -> sign == GT || sign == EQ) points

    finishPaper x = if length x > 2 then [Paper x] else []

  in
  map normalizePaper (finishPaper pl <> finishPaper pr)


-- | Mirror point @p@ against line @p1@-@p2@
mirrorPoint :: Line -> MPoint -> MPoint
mirrorPoint (Line p1 p2) p'@(finalXY -> p) =
  let
    perp = closestPointOnLine p1 p2 p
  in
  moveMPoint p' $ p `plusV` (2 `mulSV` (perp `minusV` p))


-- | Mirrors paper @p@ against line @p1@-@p2@
mirrorPaper :: Paper -> Line  -> Paper
mirrorPaper Paper{..} l =
  normalizePaper $ Paper $ reverse $ map (mirrorPoint l) paper_vertices


-- movePaper :: Paper -> Types.Point -> Types.Point -> Paper
-- movePaper Paper{..} psrc pdst =
--   Paper $
--   flip map paper_vertices $ \v ->
--     moveMPoint

-- Converts set of papers to a Solution
toSolution :: [Paper] -> Solution
toSolution ps =
  let

    sourcePoints =
      nub $ flip concatMap ps $ \Paper{..} -> paper_vertices

    finalPoints =
      map finalXY sourcePoints

    finalFacets :: [[Int]]
    finalFacets =
      nub $ flip map ps $ \Paper{..} ->
        map (fromMaybe err . flip elemIndex sourcePoints) paper_vertices

    err = error "assert: no such source point"

  in
  Solution
    (map (\(sourceXY -> (x,y)) -> Types.Point (Coord x) (Coord y)) sourcePoints)
    finalFacets
    (map (\(finalXY -> (x,y)) -> Types.Point (Coord x) (Coord y)) sourcePoints)



{-
-- Test
-- Simulate example from 'Solution specification'
--
-- ghci>  map finalXY $ paper_vertices p4
-- [(0 % 1,1 % 2),(0 % 1,0 % 1),(1 % 1,0 % 1),(1 % 2,1 % 2)]
-- ^ трапеция
--
-- ghci> > map finalXY $ paper_vertices p3'
-- [(1 % 2,1 % 2),(0 % 1,1 % 2),(0 % 1,0 % 1)]
-- ^ загнутый верхний угол
--
-- sourceXY хранят их исходные позиции

testLine1 = Line (1,0) (0,1)
(testPoint1, testPoint2) = cutPaper initialPaper testLine1
testPoint2' = mirrorPaper testPoint2 testLine1

testLine2 = Line (0,0.5) (1,0.5)
(testPoint3, testPoint4) = cutPaper testPoint2' testLine2
testPoint3' = mirrorPaper testPoint3 testLine2
-}

{-
-- | Fold in half vertically (along horizontal line)
-- | assuming all the input papres are rectangular and have the same size
-- | (as a result of another fold in half, for instance).
foldInHalfV :: [Paper] -> [Paper]
foldInHalfV ps@(p1:_) = let
  ymin = minimum $ map py $ paper_vertices p1
  ymax = maximum $ map py $ paper_vertices p1
  ymid = ymin + (ymax - ymin) / 2
  xmin = minimum $ map px $ paper_vertices p1
  xmax = maximum $ map px $ paper_vertices p1
  line = Line (xmin, ymid) (xmax, ymid)
  (uppers, lowers) = unzip $ map (`cutPaper` line) ps
  in map (`mirrorPaper` line) uppers ++ lowers

-- | Fold in half horizontally (along vertical line)
-- | assuming all the input papres are rectangular and have the same size
-- | (as a result of another fold in half, for instance).
foldInHalfH :: [Paper] -> [Paper]
foldInHalfH ps@(p1:_) = let
  ymin = minimum $ map py $ paper_vertices p1
  ymax = maximum $ map py $ paper_vertices p1
  xmin = minimum $ map px $ paper_vertices p1
  xmax = maximum $ map px $ paper_vertices p1
  xmid = xmin + (xmax - xmin) / 2
  line = Line (xmid, ymin) (xmid, ymax)
  (lefts, rights) = unzip $ map (`cutPaper` line) ps
  in lefts ++ map (`mirrorPaper` line) rights


-----------------------------------------------
-- Helper functions to generate overfolded task

foldInHalfNFrom :: Paper -> Int -> [Paper]
foldInHalfNFrom seed n = iters !! n
  where
    iters = iterate (foldInHalfH . foldInHalfV) [seed]

foldInHalfN :: Int -> [Paper]
foldInHalfN = foldInHalfNFrom initialPaper

foldTinyCorner :: Paper -> [Paper]
foldTinyCorner p = let
  xmin = minimum $ map px $ paper_vertices p
  ymin = minimum $ map py $ paper_vertices p
  line = Line (xmin, ymin + 1 % 300) (xmin + 1 % 200, ymin)
  (one, another) = cutPaper p line
  in [mirrorPaper one line, another]

foldNastyShape :: [Paper]
foldNastyShape = corner ++ tail sheets
  where
    sheets = foldInHalfN 100
    corner = foldTinyCorner $ head sheets

foldNastyShape2 :: [Paper]
foldNastyShape2 = corner : sheets
  where
    [corner, rest] = foldTinyCorner initialPaper
    sheets = foldInHalfNFrom rest 100
-}
