-- | An example module.
module Lib
        ( exampleMain
        , solverMain
        , trivial
        , almostSquareDisc
        , almostSquareDisc2
        , simpleDoubleArrow
        ) where

import Types
import Helpers (solutionLength)
import Huzita
import Solution.Trivial

import Data.Ratio

import System.Environment
import System.Exit

solverUsage :: IO ()
solverUsage = putStrLn "Usage: solve <problem>"

solverMain :: (Problem -> Solution) -> IO ()
solverMain solver = do
  args <- getArgs
  case args of
    [problemFile] -> do
      eproblem <- parse problemParser problemFile <$> readFile problemFile
      case eproblem of
        Left err -> do
          putStrLn $ "Cannot parse " ++ problemFile
          print err
          solverUsage
          exitFailure
        Right problem -> do
          let solution = solver problem
          if solutionLength solution < 5000
            then print solution >> exitSuccess
            else putStrLn "Solution is too long" >> exitFailure
    _ -> solverUsage >> exitFailure

-- | An example function.
exampleMain :: IO ()
exampleMain = return ()


fold8stripe :: Huzita ()
fold8stripe = let
  mkvline r = Line (pt r 0) (pt r 1)
  vline2 = mkvline (1%2)
  vline4 = mkvline (1%4)
  vline8 = mkvline (1%8)
  in do
    fold vline2
    fold vline4
    fold vline8
    return ()

fold16stripe :: Huzita ()
fold16stripe = let
  mkvline r = Line (pt r 0) (pt r 1)
  vline16 = mkvline (1%16)
  in do
    fold8stripe
    fold vline16
    return ()

fold32stripe :: Huzita ()
fold32stripe = let
  mkvline r = Line (pt r 0) (pt r 1)
  vline32 = mkvline (1%32)
  in do
    fold16stripe
    fold vline32
    return ()

fold64stripe :: Huzita ()
fold64stripe = let
  mkvline r = Line (pt r 0) (pt r 1)
  vline64 = mkvline (1%64)
  in do
    fold32stripe
    fold vline64
    return ()

simpleDoubleArrow :: Huzita ()
simpleDoubleArrow = let
  dline1 = Line (pt (1%8) (1%2 + 2%8)) $ pt 0 (1%2 + 3%8)
  dline2 = Line (pt (1%8) (1%2 + 2%8)) $ pt 0 (1%2 + 1%8)
  dline3 = Line (pt 0 (1%2)) $ pt (1%8) (1%2 + 1%8)
  dline4 = Line (pt 0 (2%8)) $ pt (1%8) (1%8)
  dline5 = Line (pt 0 (2%8)) $ pt (1%8) (3%8)
  dline6 = Line (pt 0 (3%8)) $ pt (1%8) (1%2)
  in do
    fold16stripe
    fold dline1
    fold dline4
    fold dline2
    fold dline5
    -- fold dline3
    -- fold dline6
    end

almostSquareDisc :: Huzita ()
almostSquareDisc = let
  -- dline1 = Line (pt 0 (1%9)) $ pt (1%8) (1%8 + 1%9) -- uneven fractions for additional trickery
  dline1 = Line (pt 0 (1%8)) $ pt (1%8) (2%8)
  dline2 = Line (pt 0 (1%2)) $ pt (1%8) (3%8)
  dline3 = Line (pt 0 (1%2 + 1%8)) $ pt (1%8) (1%2 + 2%8)
  in do
    fold8stripe
    fold dline1
    fold dline2
    fold dline3
    end

almostSquareDisc2 :: Huzita ()
almostSquareDisc2 = let
  -- dline1 = Line (pt 0 (1%9)) $ pt (1%8) (1%8 + 1%9) -- uneven fractions for additional trickery
  dline1 = Line (pt 0 (1%8)) $ pt (1%8) (2%8)
  dline2 = Line (pt 0 (1%2)) $ pt (1%8) (3%8)
  dline3 = Line (pt 0 (1%2 + 1%8)) $ pt (1%8) (1%2 + 2%8)
  in do
    fold32stripe
    fold dline1
    fold dline2
    fold dline3
    end
