{-# LANGUAGE RecordWildCards #-}
-- | Gloss import helper
module Gloss (
    module Gloss
  , module Graphics.Gloss.Geometry.Line
  , module Graphics.Gloss.Geometry.Angle
  , module Graphics.Gloss.Data.Vector
  , module Graphics.Gloss.Data.Types
  ) where

import Graphics.Gloss.Data.Types
import Graphics.Gloss.Data.Vector
import Graphics.Gloss.Geometry.Line
import Graphics.Gloss.Geometry.Angle

import Types(Coord(..))
import qualified Types as T


p2gl :: T.Point -> Point Rational
p2gl T.Point{..} = (unCoord getX, unCoord getY)

gl2p :: Point Rational -> T.Point
gl2p (getX, getY) = T.Point (Coord getX) (Coord getY)

