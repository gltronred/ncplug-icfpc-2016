{-# LANGUAGE NoMonomorphismRestriction
           , FlexibleContexts
           , LambdaCase
           , TemplateHaskell
           , RecordWildCards
           , RankNTypes
           , ViewPatterns
           , NondecreasingIndentation
           #-}

import Diagrams.Prelude hiding (Point)
import qualified Diagrams.Prelude as Dia
import qualified Diagrams.Backend.SVG as Dia
import Debug.Trace
import Data.List
import Data.Typeable
import qualified Data.Vector as V

import System.Environment
import System.Exit
import System.Process
import System.IO
-- import System.FilePath

import Control.Monad.Trans
import Control.Monad.State.Strict
import Control.Monad.Reader
import Control.Monad.Writer.Strict (MonadWriter(..), tell, execWriter)
import Control.Monad.Cont
import Control.Monad.Identity
import Control.Lens (Lens', Lens, makeLenses, (<%=), (%=), (%%=), (^.), zoom, set, view, use, uses, _1, _2, _3, _4, _5, _6)

import qualified Gloss
import Types as T
import Huzita.Monad
import Huzita.Interpreter2
import PaperFold1 hiding (p1,p2)


data HState = HState {
    _idx1 :: Int
  , _idx2 :: Int
  , _problem_idx :: Int
  , _origami :: Origami
  , _hist :: [Origami]
  } deriving(Show)

$(makeLenses ''HState)

initialState = HState 0 0 0 initialOrigami []


allPoints :: Origami -> [Point]
allPoints Origami{..} =
  nub $
  flip concatMap papers $
    \Paper{..} ->
      let
        v = paper_vertices
        edjes = (v `zip` ((tail v) <> [head v]))

        scaleS n (Point x y) = Point (n*x) (n*y)
      in
      flip concatMap edjes $ \ (hpoint -> p1, hpoint -> p2) ->
        [p1, p1 + (0.5 `scaleS` (p2 - p1)) , p2]

renderProblem prob = opacity 0.3 (stroke silu # fc yellow)  <> stroke skel # lc blue   -- # fc blue
    where
      pointConv (Point (Coord x) (Coord y)) = p2 (fromRational x, fromRational y)

      silu = V.foldl1' (<>) $ V.map draw_poly (polygons $ problemSilhouette prob)
      skel = V.foldl1' (<>) $ V.map draw_seg (segments $ problemSkeleton prob)

      draw_seg (T.Line pt1 pt2) = pathFromTrailAt (fromVertices [pointConv pt1, pointConv pt2] # wrapLine)  (pointConv pt1)
      draw_poly poly = let vs = V.toList $ vertices poly in
                         pathFromTrailAt (fromVertices (map pointConv vs) # closeLine # wrapLoop ) (pointConv $ head vs)

renderState :: (RealFloat n, Typeable n, Renderable (Path V2 n) b) =>
     HState -> QDiagram b V2 n Any
renderState HState{..} = opacity 0.3 $
      (stroke silu # fc yellow)
      <> (stroke dot1 # fc white)
      <> (stroke dot2 # fc black)
      -- <> (stroke (draw_dot pl) # fc black)
      -- <> (stroke (draw_dot pr) # fc white)
    where

      {- Utils -}

      colorScheme = cycle [red, yellow, green, cyan, blue, purple]

      pointConv MPoint{..} = p2 (fromRational px, fromRational py)
      pointConv2 (Point (Coord x) (Coord y)) = p2 (fromRational x, fromRational y)

      ps = allPoints _origami

      Origami{..} = _origami

      cp1 = (ps !! _idx1)

      cp2 = (ps !! _idx2)

      pl = leftPoint (T.Line cp1 cp2)
      pr = rightPoint (T.Line cp1 cp2)

      {- Main part -}

      draw_dot x = pathFromTrailAt (circle 0.04) (pointConv2 x)

      dot1 = draw_dot cp1
      dot2 = draw_dot cp2

      silu = foldl1' (<>) $ map draw_poly papers

      draw_poly Paper{..} =
        let
          vs = paper_vertices
        in
        pathFromTrailAt (fromVertices (map pointConv vs) # closeLine # wrapLoop ) (pointConv $ head vs)


rightPoint :: T.Line -> Point
rightPoint (T.Line p1 p2) =
  p1 + rotateP (pi/2) (p2 - p1)

leftPoint :: T.Line -> Point
leftPoint (T.Line p1 p2) =
  p1 - rotateP (pi/2) (p2 - p1)

rotateP :: Double -> Point -> Point
rotateP dbl (Point (Coord x) (Coord y)) =
  let
    (x',y') = Gloss.rotateV dbl (fromRational x, fromRational y)
  in
  Point (Coord $ toRational x') (Coord $ toRational y')

main :: IO ()
main = do
  hSetBuffering stdin NoBuffering

  let outfile = "out.svg"
  let porbfile = "1.txt"

  -- problems <- map ("problems"</>) . filter (".txt"`isSuffixOf`) <$> getDirectoryContents d

  Right p <- parseProblem "problems/24.txt"

  flip evalStateT initialState $ do
  forever $ do
    ps <- allPoints <$> use origami

    -- get >>= traceM . show
    get >>= \s -> do
      liftIO $ Dia.renderSVG outfile (dims2D (500 :: Float) 500) $ frame 0.5 $
        renderState s <> renderProblem p

    liftIO $ putStrLn "j|k|J|K - move points; f|F - folds; u - undo"

    let

      inc,dec :: (MonadState HState m) => Int -> Lens' HState Int -> m ()

      inc max idx = do
        i <- use idx
        if (i >= max) then
            idx %= const 0
        else
            idx %= (+1)

      dec max idx = do
        i <- use idx
        if (i <= 0) then do
            idx %= const max
        else do
            idx %= (+(-1))

    c <- liftIO getChar

    case c of
      'j' -> inc (length ps-1) idx1
      'k' -> dec (length ps-1) idx1
      'J' -> inc (length ps-1) idx2
      'K' -> dec (length ps-1) idx2

      -- фолды другие, у Fold поменялась сигнатура и смысл (см. Huzita.Monad)
      'f' -> do
        p1 <- (ps !!) <$> use idx1
        p2 <- (ps !!) <$> use idx2
        let l = T.Line p1 p2
        use origami >>= \o -> do
          hist %= (o:)
        origami %= performFold l

      'F' -> do
        p1 <- (ps !!) <$> use idx1
        p2 <- (ps !!) <$> use idx2
        let l = T.Line p2 p1
        use origami >>= \o -> do
          hist %= (o:)
        origami %= performFold l

      'u' -> do
        hs <- use hist
        case hs of
          (h:hs) -> do
            origami %= const h
            hist %= const hs
          [] -> return ()
      's' -> do
        Origami{..} <- use origami
        let s = toSolution papers
        liftIO $ putStrLn $ show s

      _ -> return ()


