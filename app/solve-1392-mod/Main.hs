import qualified Lib

import Solution.S1392
import Huzita

main :: IO ()
main = Lib.solverMain (solveHuzita1 . solve1392Main)
