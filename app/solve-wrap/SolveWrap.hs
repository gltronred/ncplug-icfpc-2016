import qualified Lib

import Solution.Wrapper
import Huzita

main :: IO ()
main = Lib.solverMain (solveHuzita1 . solverWrap)
