{-# LANGUAGE NoMonomorphismRestriction,
             FlexibleContexts #-}

import Diagrams.TwoD.Arrow
import Diagrams.Prelude
import Diagrams.Backend.SVG

import Types as T

import System.Environment
import System.Exit

import Debug.Trace as D

import qualified Data.Vector as V

batchOut = "render.svg"

usage :: IO ()
usage = do
  putStrLn   "Usage: ./visualizer single <problem> <outfile>"
  putStrLn $ "       ./visualizer batch [<problems>] - renders into " ++ batchOut
  putStrLn   "       ./visualizer solution <sol_file> <outfile>"
  exitFailure

pointConv (Point (Coord x) (Coord y)) = p2 (fromRational x, fromRational y)

renderProblem prob = (opacity 0.2 base) # fc blue <> opacity 0.3 (stroke silu # fc yellow)  <> skel -- # lc blue   -- # fc blue
    where
      base = stroke $ pathFromTrailAt (fromVertices ( map pointConv [ pt 0 0, pt 1 0, pt 1 1, pt 0 1]) # closeLine # wrapLoop) (p2 (0,0))
      silu = V.foldl1' (<>) $ V.map draw_poly (polygons $ problemSilhouette prob)
      skel = V.foldl1' (<>) $ V.map draw_seg (segments $ problemSkeleton prob)

      draw_seg (T.Line pt1 pt2) = D.trace ("seg " ++ show pt1 ++ " " ++ show pt2 ++ "of sqr size" ++ show (getX (pt2 - pt1) ^ 2 + getY (pt2 - pt1) ^ 2 )) $   arrowFromLocatedTrail $ (fromVertices [pointConv pt1, pointConv pt2] # wrapLine) `at` (pointConv pt1)
      draw_poly poly = let vs = V.toList $ vertices poly in
                         pathFromTrailAt (fromVertices (map pointConv vs) # closeLine # wrapLoop ) (pointConv $ head vs)

renderBatch problemFiles outFile = do
  problems_txt <- mapM readFile problemFiles
  let eproblems = sequence $ map (parse problemParser "tst") problems_txt
  case eproblems of
    Left err -> do
      putStrLn $ "Cannot parse "
      print err
      exitFailure
    Right problems -> do
      renderSVG outFile (dims2D (500 :: Float) 500) $ frame 0.5 $ hsep 0.1 (map renderProblem problems)

colorScheme = cycle [red, yellow, green, cyan, blue, purple]

renderSolution sol = hsep 0.5 [mconcat $ base_dia : faces_src_dia , mconcat faces_dst_dia]
  where
    base_dia = stroke $ pathFromTrailAt (fromVertices (map p2 [ (0,0),(1,0),(1,1),(0,1)]) # closeLine # wrapLoop) (p2 (0,0))
    faces_dst_dia = zipWith (renderFacet $ solutionDestinations sol) (solutionFacets sol) colorScheme
    faces_src_dia = zipWith (renderFacet $ solutionSources sol) (solutionFacets sol) colorScheme
    renderFacet points inds color = opacity 0.3 $ stroke (pathFromTrailAt (fromVertices (map (\i -> pointConv $ points !! i) inds) # closeLine # wrapLoop) (pointConv $ points !! head inds)) # fc color

main = do
  args <- getArgs
  case args of
    ("batch" : problemFiles) -> renderBatch problemFiles batchOut
    ["single", problemFile, outFile] -> renderBatch [problemFile] outFile
    ["solution", solFile, outFile] -> do
      esol <- parse solutionParser solFile <$> readFile solFile
      case esol of
        Right sol -> do
          renderSVG outFile (dims2D (500 :: Float) 500) $ frame 0.5 $ renderSolution sol
        Left err -> do print err
                       exitFailure
    _ -> usage

--main = print "Hello from Visualizer"
