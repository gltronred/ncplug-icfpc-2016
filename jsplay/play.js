function fold(l, z, f) {
    for (var i = 0; i < l.length; i++) z = f(z, l[i]);
    return z;
}

function push(l, x) {
    l = l.concat();
    l.push(x);
    return l;
}

var problems={

	/* From http://2016sv.icfpcontest.org/problem/view/1  */
	problem1: [
		[
			[[0,0],[1,0],[1,1],[0,1]],
		],
		[[[1/8,1/8],[1/8,.25],[.25,.25],[.25,1/8]]],
		[
			[[0,0], [1,0]],
			[[0,0], [0,1]],
			[[1,0], [1,1]],
			[[0,1], [1,1]]
		]
	],

	/* Плохо рисует - там поле зрение от 0 до 1. Видимо, нужно скейлить */
	problem2: [
		[
			[[1,1], [2,1], [2,2], [1,2]]
		],
		[],
		[
			[[1,1], [1,2]],
			[[2,1], [2,2]],
			[[1,1], [2,1]],
			[[1,2], [2,2]]
		]
	]
	,
	sample:
	[

		[
			[[0,0],[1,0],[.5,.5],[0,.5]]
		],

		[],

		[
			[[.5,.5],[0,0]],
			[[0,0],[1,0]],
			[[1,0],[.5,.5]],
			[[.5,.5],[0,.5]],
			[[0,.5],[0,0]]
		]
	]

	,convex1:[[[[0,0],[1,0],[1,1],[0,1]]],[],[[[0,1],[0,0]],[[1,0],[1,1]],[[0,0],[1,0]],[[1,1],[0,1]]]],convex2:[[[[0,0],[1,0],[1,1],[0,1]]],[],[[[0,1],[0,0]],[[1,0],[1,1]],[[0,0],[1,0]],[[1,1],[0,1]]]]
	,donut:[
		[[[0,1/8],[1/8,0],[.25,0],[3/8,1/8],[3/8,.25],[.25,3/8],[1/8,3/8],[0,.25]]],
		[[[.25,1/8],[1/8,1/8],[1/8,.25],[.25,.25]]],
		[[[3/8,1/8],[0,1/8]],[[1/8,3/8],[1/8,0]],[[1/8,0],[.25,0]],[[.25,3/8],[1/8,3/8]],[[.25,0],[3/8,1/8]],[[3/8,.25],[.25,3/8]],[[0,.25],[3/8,.25]],[[3/8,.25],[3/8,1/8]],[[.25,0],[.25,3/8]],[[1/8,3/8],[0,.25]],[[0,.25],[0,1/8]],[[1/8,0],[0,1/8]]]
	]
	,crane:[[[[.16,.72],[1831/6745,4749/6745],[49/195,224/585],[.35,.35],[224/585,49/195],[.84,.28],[448/895,294/895],[168/295,441/1180],[.7,.7],[441/1180,168/295],[294/895,448/895],[.3,.7],[277/1015,738/1015]]],[],[[[5603/14650,2777/7325],[39221/111815,38878/111815]],[[112/265,147/530],[5603/14650,2777/7325]],[[14/41,147/410],[.7,.7]],[[.7,.7],[168/295,441/1180]],[[.35,.35],[.84,.28]],[[49/195,224/585],[.35,.35]],[[147/415,28/83],[21/58,10/29]],[[39221/111815,38878/111815],[.84,.28]],[[.35,.35],[.3,.7]],[[5603/14650,2777/7325],[168/295,441/1180]],[[.5,21/64],[.5,.5]],[[21/58,10/29],[116/273,29/104]],[[2777/7325,5603/14650],[38878/111815,39221/111815]],[[21/64,.5],[2777/7325,5603/14650]],[[112/265,147/530],[147/530,112/265]],[[224/585,49/195],[.84,.28]],[[28/83,147/415],[188/635,447/635]],[[116/273,29/104],[5603/14650,2777/7325]],[[277/1015,738/1015],[.3,.7]],[[147/410,14/41],[.7,.7]],[[50378/168335,117957/168335],[38878/111815,39221/111815]],[[5603/14650,2777/7325],[.5,21/64]],[[.84,.28],[147/415,28/83]],[[10/29,21/58],[28/83,147/415]],[[.7,.7],[441/1180,168/295]],[[168/295,441/1180],[224/585,49/195]],[[.16,.72],[188/635,447/635]],[[.5,.5],[21/64,.5]],[[49/195,224/585],[441/1180,168/295]],[[2777/7325,5603/14650],[147/530,112/265]],[[441/1180,168/295],[2777/7325,5603/14650]],[[50378/168335,117957/168335],[.16,.72]],[[29/104,116/273],[2777/7325,5603/14650]],[[224/585,49/195],[.35,.35]],[[29/104,116/273],[10/29,21/58]],[[.16,.72],[277/1015,738/1015]],[[.3,.7],[.16,.72]],[[.5,.5],[.35,.35]],[[49/195,224/585],[277/1015,738/1015]]]]};






/* Not so hard to deobfuscate this code, but you will gain nothing by it. */
function Z(j, z, f) {
    if (j) {
        var b = F(z, j),
            q = W(R(j, z), [2, 0]),
            y, p;
        q = b[0] * q[0] + b[1] * q[1];
        if (b[0]) {
            y = [q / b[0], 0];
            p = [(q - b[1]) / b[0], 1]
        } else {
            if (!b[1]) return;
            y = [0, q / b[1]];
            p = [1, q / b[1]]
        }
        L(F(z, j), N(F(y, j)))[1] >= 0 && (q = y, y = p, p = q);
        q = fold(I.concat().reverse(), [], function(d, w) {
            var s = C(w[1], [
                [],
                []
            ], function(s, c, v) {
                var m = push(s[0], c),
                    g = F(p, y),
                    q = W(F(v[1], c[1]), g);
                return q[1] != 0 && (q = W(F(c[1], y), g)[1] / -q[1], 0 <= q && q < 1) ? (q = R(c[1], L(F(v[1], c[1]), [q, 0])), q = [R(L(F(v[0], c[0]), W(F(q, c[1]), F(v[1], c[1]))), c[0]), q], [push(s[1], q), push(m, q)]) : [m, s[1]]
            });
            s = s[1].length ? (L(F(p, y), N(F(s[0][0][1], y)))[1] >= 0 && (s = s.reverse()), U(s, function(o) {
                return [w[0], C(o, [], function(s, y, p) {
                    return y[1][0] == p[1][0] && y[1][1] == p[1][1] && y[0][0] == p[0][0] && y[0][1] == p[0][1] ? s : push(s, y)
                })]
            })) : L(F(p, y), N(F(w[1][0][1], y)))[1] >= 0 ? [0, w] : [w];
            s[0] && d.unshift(s[0]);
            if (s[1]) {
                var k = F(p, y);
                k = W(k, N(k));
                d = push(d, [!s[1][0], U(s[1][1], function(t) {
                    return [t[0], R(L(N(F(t[1], y)), k), y)]
                })])
            }
            return d
        });
        if (f) {
            B(q);
            var e = $("#dst")[0].getContext("2d");
            O([j, z], function(l) {
                e.fillStyle = "#000000";
                e.fillRect(l[0] - 1 / 128, l[1] - 1 / 128, 1 / 64, 1 / 64)
            })
        } else G.push(I), I = q, E = 0, B(I)
    }
}

function B(n) {
    Q(n, 1);
    Q(n, 0)
}

function F(f, q) {
    return [f[0] - q[0], f[1] - q[1]]
}

function T(h) {
    return $("#origami #" + h + " li.active").data(h + "-id")
}

function Y(r) {
    if (r.length == 0) $("#flip").click(function() {
        I = E ? G.pop() : (G.push(I), U(I, function(w) {
            return [!w[0], U(w[1], function(z) {
                return [z[0], S(z[1])]
            })]
        }).reverse());
        E = !E;
        B(I)
    }), $("#undo").click(function() {
        if (G.length >= 1) I = G.pop();
        B(I)
    }), $.map(problems, function(s, g) {
        $("#origami #silhouette-list").append('<li data-silhouette-id="' + g + '"><a href="#">' + g + '</a></li>')
    }), O([0, 1], function(q) {
        var y = "#origami #" + (q ? "silhouette" : "texture"),
            f = $(q ? "#dst" : "#src"),
            c = f.height() / 2 + 128,
            j = f.width() / 2 - 128,
            m = 0;
        $(y + " a").click(function(d) {
            d.preventDefault();
            d = "active";
            $(y + " li").removeClass(d);
            $(this).parent().addClass(d);
            B(I)
        });
        if (q) {
            var u = function(b) {
                return [(b.offsetX - j) / 256, (c - b.offsetY) / 256]
            };
            f.mousedown(function(d) {
                $("#navi").remove();
                m = u(d);
                return !1
            });
            f.mousemove(function(d) {
                Z(m, u(d), 1);
                return !1
            });
            f.mouseup(function(d) {
                Z(m, u(d));
                m = 0;
                return !1
            })
        }
        f[0].getContext("2d").setTransform(256, 0, 0, -256, j, c)
    }), B(I);
    else {
        var o = new Image();
        J.push(o);
        o.onload = function() {
            Y(r)
        };
        o.src = "./static/texture" + r.shift() + ".png"
    }
}

function S(e) {
    return [1 - e[0], e[1]]
}

function R(t, m) {
    return [t[0] + m[0], t[1] + m[1]]
}

function Q(i, e) {
    var k = $(e ? "#dst" : "#src")[0].getContext("2d");
    k.clearRect(-1, -1, 3, 3);
    k.beginPath();
    k.lineWidth = .001;
    k.strokeStyle = "#000000";
    fold(Array.apply(null, {
        length: 40
    }), -10, function(a, o) {
        k.moveTo(-1, o = a / 10);
        k.lineTo(3, o);
        k.moveTo(o, -1);
        k.lineTo(o, 3);
        return a + 1
    });
    k.stroke();
    O(i, function(v) {
        k.beginPath();
        var g = v[1];
        O(g, function(y) {
            k.lineTo(y[e][0], y[e][1])
        });
        k.closePath();
        var j = T("texture");
        j && (e && v[0] ? (k.save(), k.clip(), k.translate(g[0][1][0], g[0][1][1]), i = S(g[0][0]), g = W(F(S(g[1][0]), i), F(g[1][1], g[0][1])), k.rotate(-Math.atan2(g[1], g[0])), k.translate(-i[0], -i[1]), k.drawImage(J[j - 1], 0, 0, 512, 512, 0, 0, 1, 1), k.restore()) : (k.fillStyle = "#cccccc", k.fill()));
        k.lineWidth = .003;
        k.strokeStyle = "#000000";
        k.stroke()
    });
    if (e) {
        var z = T("silhouette");
        if (z = z ? problems[z] : null) k.beginPath(), O([z[0], z[1]], function(n) {
            O(n, function(l) {
                O(l, function(y) {
                    k.lineTo(y[0], y[1])
                });
                k.closePath()
            })
        }), k.fillStyle = "rgba(255,192,192,.8)", k.fill(), k.beginPath(), O(z[2], function(u) {
            k.moveTo(u[0][0], u[0][1]);
            k.lineTo(u[1][0], u[1][1])
        }), k.lineWidth = .005, k.strokeStyle = "rgba(255,128,128,.8)", k.stroke()
    }
}

function C(n, l, k) {
    l = fold(n, [l], function(l, v) {
        return [l[1] ? k(l[0], l[1], v) : l[0], v]
    });
    return k(l[0], l[1], n[0])
}

function L(g, t) {
    return [g[0] * t[0] - g[1] * t[1], g[0] * t[1] + g[1] * t[0]]
}

function W(p, x) {
    var d = x[0] * x[0] + x[1] * x[1];
    return [(x[0] * p[0] + x[1] * p[1]) / d, (x[0] * p[1] - x[1] * p[0]) / d]
}

function U(q, n) {
    return fold(q, [], function(w, y) {
        return push(w, n(y))
    })
}

function N(y) {
    return [y[0], -y[1]]
}

function O(c, z) {
    fold(c, 0, function(t, x) {
        z(x)
    })
}
var I = [
        [!1, [
            [
                [0, 0],
                [0, 0]
            ],
            [
                [1, 0],
                [1, 0]
            ],
            [
                [1, 1],
                [1, 1]
            ],
            [
                [0, 1],
                [0, 1]
            ]
        ]]
    ],
    G = [],
    E = 0,
    J = [];
$(function() {
    Y([0, 1, 2, 3])
});


