module SolverTests where

import Test.Tasty.Hspec
import qualified Data.Vector as V

import Helpers
import Types
import Huzita
import Huzita.Interpreter2

import Solution.Wrapper

problem = Problem (Silhouette $ V.fromList [Polygon True $ V.fromList pts]) (Skeleton $ V.fromList [])
  where pts = [pt 10 10, pt 10.8 10, pt 10.8 10.8, pt 10 10.8]

solverTests :: Spec
solverTests = describe "Brick test" $ do
  describe "Huzita1" $ brickTest solveHuzita1
  describe "Huzita2" $ brickTest solveHuzita2

brickTest solver = do
  let sol = solver . solverWrap $ problem
  let bp@(p1,p2) = boundingPair (solutionDestinations sol)
  it "should be wrapped tyghtly" $ do
    p1 `shouldBe` pt 10.0 10.0
    p2 `shouldBe` pt 10.8 10.8
