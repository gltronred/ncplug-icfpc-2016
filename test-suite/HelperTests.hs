{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module HelperTests where

import Test.Tasty.Hspec
import qualified Test.Tasty.QuickCheck as QC
import Types
import Helpers

import Data.Ratio
import qualified Data.Vector as V

deriving instance QC.Arbitrary Coord


square :: Polygon
square = Polygon False $ V.fromList
         [ Point (Coord $ -1 % 2) (Coord $ -1 % 2)
         , Point (Coord $  1 % 2) (Coord $ -1 % 2)
         , Point (Coord $  1 % 2) (Coord $  1 % 2)
         , Point (Coord $ -1 % 2) (Coord $  1 % 2)
         ]

concave_square = Polygon False $ V.fromList
         [ Point (Coord 0) (Coord 0)
         , Point (Coord 1) (Coord 0)
         , Point (Coord 0.5) (Coord 0.5)
         , Point (Coord 1) (Coord 1)
         , Point (Coord 0) (Coord 1)
         ]
concave_square2 = Polygon False $ V.fromList
         [ Point (Coord 0) (Coord 0)
         , Point (Coord 1) (Coord 0)
         , Point (Coord 1) (Coord 1)
         , Point (Coord 0.5) (Coord 0.5)
         , Point (Coord 0) (Coord 1)
         ]

exampleSolution :: Solution
exampleSolution = solution
  where
    Right solution = parseSolution $ unlines [
      "7",
      "0,0",
      "1,0",
      "1,1",
      "0,1",
      "0,1/2",
      "1/2,1/2",
      "1/2,1",
      "4",
      "4 0 1 5 4",
      "4 1 2 6 5",
      "3 4 5 3",
      "3 5 6 3",
      "0,0",
      "1,0",
      "0,0",
      "0,0",
      "0,1/2",
      "1/2,1/2",
      "0,1/2",
      ""
      ]


helpersTests :: Spec
helpersTests = describe "Tests for helpers" $ do
  it "should compute area" $ do
    area square `shouldBe` 1
  it "should shift square" $ do
    shift (Point (Coord $ 1%2) (Coord $ 1%2)) square `shouldBe` Polygon False (V.fromList
                                                                               [ Point 0 0
                                                                               , Point 1 0
                                                                               , Point 1 1
                                                                               , Point 0 1
                                                                               ])
  it "area doesn't depend on shift" $ QC.property $
    \x y -> area (shift (Point x y) square) QC.=== 1
  it "should compute correst solution length" $ do
    solutionLength exampleSolution `shouldBe` 78
  it "should compute concave area (convex)" $ do
    area_concave square `shouldBe` 1
  it "should compute concave area (concave)" $ do
    area_concave concave_square `shouldBe` (0.25+0.25+0.25)
    area_concave concave_square2 `shouldBe` (0.25+0.25+0.25)


