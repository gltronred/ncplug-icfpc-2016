{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module HuzitaInterpreter1 where

import Test.Tasty.Hspec
import qualified Test.Tasty.QuickCheck as QC

import Types
import Huzita
import Huzita.Interpreter1
import qualified Huzita.Interpreter2 as HI2

import Data.List (sort)
import Data.Ratio

deriving instance QC.Arbitrary Coord
instance QC.Arbitrary Point where
  arbitrary = Point <$> QC.arbitrary <*> QC.arbitrary
instance QC.Arbitrary Line where
  arbitrary = Line <$> QC.arbitrary <*> QC.arbitrary

paper = [pt 0 0, pt 1 0, pt 1 1, pt 0 1]

helpers_tests :: Spec
helpers_tests =
  describe "Helpers" $ do
    let len p = getX p^2 + getY p^2
    it "switchAxis (inverseAxis (a,b)) . switchAxis (a,b) == id" $ QC.property $
      \a b p -> (\((a,b),(c,d)) -> a*d-b*c /= 0) a QC.==>
                switchAxis (inverseAxis (a,b)) (switchAxis (a,b) p) == p
    it "Calculates line equation" $ do
      toParamLine (Line (pt 0 0) (pt 1 1)) `shouldBe` (-1,1,0)
      toParamLine (Line (pt 0 1) (pt 1 3)) `shouldBe` (-2,1,-1)
      toParamLine (Line (pt 0 1) (pt 1 0)) `shouldBe` (1,1,-1)
      toParamLine (Line (pt 2 1) (pt 1 2)) `shouldBe` (-1,-1,3)
      toParamLine (Line (pt 0 1) (pt 1 1)) `shouldBe` (0,1,-1)

    it "Line ends are on line" $ QC.property $
      \p1 p2 -> map (sign $ toParamLine (Line p1 p2)) [p1,p2] == [EQ,EQ]

    it "Sign works, 1,0.5 -> 0,0.5" $ do
      let line = toParamLine $ Line (pt 1 0.5) (pt 0 0.5)
      sign line (pt 0.25 0.25) `shouldBe` GT
      sign line (pt 0.75 0.25) `shouldBe` GT
      sign line (pt 0.5 0.5) `shouldBe` EQ
      sign line (pt 0.25 0.75) `shouldBe` LT
      sign line (pt 0.75 0.75) `shouldBe` LT

    it "Reflexion matrix works, y=x" $ do
      let ref_x = reflMatrix $ toParamLine (Line (pt 0 0) (pt 1 1))
      apply ref_x (pt 1 0) `shouldBe` (pt 0 1)
    it "Reflexion matrix works, y=-x" $ do
      let ref_x = reflMatrix $ toParamLine (Line (pt 0 0) (pt 1 (-1)))
      apply ref_x (pt 1 0) `shouldBe` (pt 0 (-1))
      apply ref_x (pt 1 (-1)) `shouldBe` (pt 1 (-1))

    it "Reflects around x" $ do
      let ref_x = reflectAcross (Line (pt 0 0) (pt 1 0)) axisE
      switchAxis ref_x (pt 1 1) `shouldBe` (pt 1 (-1))

    it "Reflects around y" $ do
      let ref_y = reflectAcross (Line (pt 0 0) (pt 0 1)) axisE
      switchAxis ref_y (pt 1 1) `shouldBe` (pt (-1) 1)

    it "Reflects around x=y" $ do
      let ref_xy = reflectAcross (Line (pt 0 0) (pt 1 1)) axisE
      switchAxis ref_xy (pt 1 0) `shouldBe` (pt 0 1)

    it "Reflects around y=1" $ do
      let ref_y1 = reflectAcross (Line (pt 0 1) (pt 1 1)) axisE
      switchAxis ref_y1 (pt 1 0) `shouldBe` (pt 1 2)
    it "Reflect around x=0 then around y = x-1" $ do -- была ошибка в уравнении
      let line0 = Line (pt 0 $ -1) (pt 0 1)
          line1 = Line (pt 1 0) (pt 2 1)
          axis0 = (eye, pt 0 0)
          axis1 = reflectAcross line0 axis0
          axis2 = reflectAcross line1 axis1
      switchAxis axis2 (pt 0 0) `shouldBe` pt 1 (-1)
      switchAxis axis2 (pt 2 1) `shouldBe` pt 2 (-3)
      switchAxis axis2 (pt (-1) 0) `shouldBe` pt 1 (0)
      switchAxis axis2 (pt (-1) 1) `shouldBe` pt 2 (0)
-- полагаю, неправильно посчитаны координаты
--      map (switchAxis axis2) [pt 0 0, pt 1.5 0.5, pt 0 0.5, pt 2 0.5, pt 2 $ -0.5] `shouldBe`
--            [pt 1 $ -1, pt 0.5 0.5, pt 0.5 $ -1, pt 0.5 1, pt 1.5 1]
    it "Reflect around x=0 then around y = 0" $ do
      let line0 = Line (pt 0 $ -1) (pt 0 1)
          line1 = Line (pt (-1) 0) (pt 1 0)
          axis0 = (eye, pt 0 0)
          axis1 = reflectAcross line0 axis0
          axis2 = reflectAcross line1 axis1
      switchAxis axis2 (pt (-1) (-2)) `shouldBe` pt 1 2
    it "Intersects by ver line correctly" $ do
      let (poly1, poly2) = intersectPoly (Line (pt 0.5 0) (pt 0.5 1)) paper
      sort poly1 `shouldBe` sort [pt 0 0, pt 0.5 0, pt 0.5 1, pt 0 1]
      (sort <$> poly2) `shouldBe` Just (sort [pt 0.5 0, pt 1 0, pt 1 1, pt 0.5 1])
    it "Intersects correctly void" $ do
      let (poly1, poly2) = intersectPoly (Line (pt 2 0) (pt 3 2)) paper
      sort poly1 `shouldBe` sort paper
      (sort <$> poly2) `shouldBe` Nothing
    it "Intersects by diag line correctly" $ do
      let (poly1, poly2) = intersectPoly (Line (pt 1 0.5) (pt 0.5 1)) paper
      sort poly1 `shouldBe` sort [pt 0 0, pt 1 0, pt 1 0.5, pt 0.5 1, pt 0 1]
      (sort <$> poly2) `shouldBe` Just (sort [pt 1 0.5, pt 1 1, pt 0.5 1])
    it "Intersects by diag line going through points correctly" $ do
      let (poly1, poly2) = intersectPoly (Line (pt 1 0) (pt 0 1)) paper
      sort poly1 `shouldBe` sort [pt 0 0, pt 1 0, pt 0 1]
      (sort <$> poly2) `shouldBe` Just (sort [pt 0 1, pt 1 0, pt 1 1])

    it "Intersects triangle by its base" $ do
      -- facet from task2'
      let poly = [Point {getX = 1/2, getY = 2/3},Point {getX = 5/6, getY = 1},Point {getX = 1/6, getY = 1}]
          axis = (((-1,0),(0,-1)),Point {getX = 1, getY = 4/3})
          line = Line (pt 0 $ 1%3) (pt 1 $ 1%3)
          l = switchAxis' axis line
          (p1, mp2) = intersectPoly l poly
      (sort p1, mp2) `shouldBe` (sort poly, Nothing)

    it "Gets center of triangle" $ do
      let poly = [Point {getX = 1/2, getY = 2/3},Point {getX = 5/6, getY = 1},Point {getX = 1/6, getY = 1}]
      center poly `shouldBe` pt (1%2) (8%9)

    it "Gets new facets from triangle by its base" $ do
      -- facet from task2'
      let facet = ([Point {getX = 1/2, getY = 2/3},Point {getX = 5/6, getY = 1},Point {getX = 1/6, getY = 1}],(((-1,0),(0,-1)),Point {getX = 1, getY = 4/3}))
          line = Line (pt 0 $ 1%3) (pt 1 $ 1%3)
      sort' (map fst $ getNewFacets line facet) `shouldBe` sort' (map fst [ facet ])

    it "Sorts CCW" $ do
      let poly = [pt 0 0, pt 0 1, pt 1 0, pt 1 1]
      toCCW poly `shouldBe` [pt 0 0, pt 1 0, pt 1 1, pt 0 1]

    it "Square root" $ do
      squareRoot 25 `shouldBe` 5
      squareRoot 1 `shouldBe` 1

    it "toSolution sorts facet CCW" $ do
      let poly = [pt 0 0, pt 0 1, pt 1 0, pt 1 1]
          axis = (eye, pt 0 0)
          facet = (poly, axis)
          origami = Origami [] [] [facet]
          sol = toSolution origami
          [res] = map (map (solutionSources sol!!)) (solutionFacets sol)
      take 4 (dropWhile (/= pt 0 0) $ cycle res) `shouldBe` [pt 0 0, pt 1 0, pt 1 1, pt 0 1]


sort' = sort . map sort
faces_id_to_pt sol = sort' $ map (\facet -> map (solutionSources sol !!) facet) (solutionFacets sol)

tst_huzita_void :: (Huzita () -> Solution) -> Spec
tst_huzita_void solver = describe "VoidTask" $ let
  sol = solver (return ())
  init_sheet = sort [pt 0 0, pt 1 0, pt 1 1, pt 0 1]
  in do
  it "should preserve sources" $ do
    sort (solutionSources sol) `shouldBe` init_sheet
  it "should preserve facets" $ do
    faces_id_to_pt sol `shouldBe` sort' [ init_sheet ]
  it "should preserve destinations" $ do
    sort (solutionDestinations sol) `shouldBe` init_sheet

task_single_fold :: Huzita ()
task_single_fold = do
  let p1 = pt 1 0
      p2 = pt 0 1
      p = pt 1 1
  fold (Line p1 p2)
  end

task_single_fold2 :: Huzita ()
task_single_fold2 = do
  let p1 = pt 1 0.5
      p2 = pt 0.5 1
      p = pt 1 1
  fold (Line p1 p2)
  end

task_single_fold3 :: Huzita ()
task_single_fold3 = fold (Line (pt 0.5 0) (pt 0.5 1)) >> end

task_single_fold4 :: Huzita ()
task_single_fold4 = fold (Line (pt 0 0.5) (pt 1 0.5)) >> end

tst_single_fold :: (Huzita () -> Solution) -> Spec
tst_single_fold solver = describe "Single fold on existing points" $ let
  sol = solver task_single_fold
  init_sheet = sort [pt 0 0, pt 1 0, pt 1 1, pt 0 1]
  in do
  it "should preserve sources" $ do
    sort (solutionSources sol) `shouldBe` init_sheet
  it "should have two facets" $ do
    length (solutionFacets sol) `shouldBe` 2
    faces_id_to_pt sol  `shouldBe` sort' [ [pt 0 0, pt 1 0, pt 0 1], [pt 1 1, pt 1 0, pt 0 1] ]
  it "should glue destinations" $ do
    sort (solutionDestinations sol) `shouldBe` sort [pt 0 0, pt 1 0, pt 0 1, pt 0 0]

tst_single_fold2 :: (Huzita () -> Solution) -> Spec
tst_single_fold2 solver = describe "Single fold on new points" $ let
  sol = solver task_single_fold2
  init_sheet = sort [pt 0 0, pt 1 0, pt 1 1, pt 0 1]
  in do
  it "should preserve sources" $ do
    sort (solutionSources sol) `shouldBe` sort [pt 0 0, pt 1 0, pt 1 0.5, pt 1 1, pt 0.5 1, pt 0 1]
  it "should have two facets" $ do
    length (solutionFacets sol) `shouldBe` 2
    faces_id_to_pt sol  `shouldBe` sort' [ [pt 0 0, pt 1 0, pt 1 0.5, pt 0.5 1, pt 0 1], [pt 1 1, pt 1 0.5, pt 0.5 1] ]
  it "should move single corner" $ do
    sort (solutionDestinations sol) `shouldBe` sort [pt 0 0, pt 1 0, pt 0 1, pt 1 0.5, pt 0.5 1, pt 0.5 0.5]

tst_single_fold3 :: (Huzita () -> Solution) -> Spec
tst_single_fold3 solver = describe "Single fold vertically (first part of double_fold)" $ let
  sol = solver task_single_fold3
  init_sheet = sort [pt 0 0, pt 1 0, pt 1 1, pt 0 1]
  in do
  it "should preserve sources" $ do
    sort (solutionSources sol) `shouldBe` sort [pt 0 0, pt 1 0, pt 0.5 0, pt 1 1, pt 0.5 1, pt 0 1]
  it "should have two facets" $ do
    length (solutionFacets sol) `shouldBe` 2
    faces_id_to_pt sol  `shouldBe` sort' [ [pt 0 0, pt 0.5 0, pt 0.5 1, pt 0 1], [pt 0.5 0, pt 0.5 1, pt 1 1, pt 1 0] ]
  it "has correct dsts" $ do
    sort (solutionDestinations sol) `shouldBe` sort [pt 0 0, pt 0.5 0, pt 0.5 1, pt 0 1, pt 0 0, pt 0 1]

tst_single_fold4 :: (Huzita () -> Solution) -> Spec
tst_single_fold4 solver = describe "Single fold horizontally (second part of double_fold)" $ let
  sol = solver task_single_fold4
  init_sheet = sort [pt 0 0, pt 1 0, pt 1 1, pt 0 1]
  in do
  it "should preserve sources" $ do
    sort (solutionSources sol) `shouldBe` sort [pt 0 0, pt 1 0, pt 0 0.5, pt 1 1, pt 1 0.5, pt 0 1]
  it "should have two facets" $ do
    length (solutionFacets sol) `shouldBe` 2
    faces_id_to_pt sol  `shouldBe` sort' [ [pt 0 0, pt 0 0.5, pt 1 0.5, pt 1 0], [pt 0 0.5, pt 1 0.5, pt 1 1, pt 0 1] ]
  it "has correct dsts" $ do
    sort (solutionDestinations sol) `shouldBe` sort [pt 0 1, pt 0 0.5, pt 1 0.5, pt 1 1, pt 0 1, pt 1 1]

tsk_double_fold :: Huzita ()
tsk_double_fold = do
  fold (Line (pt 0.5 0) (pt 0.5 1))
  fold (Line (pt 1 0.5) (pt 0 0.5))
  end

spec_double_fold :: (Huzita () -> Solution) -> Spec
spec_double_fold solver = describe "Double rectangle fold" $ let
  sol_half = solver $ (fold (Line (pt 0.5 0) (pt 0.5 1)) >> end)
  sol = solver tsk_double_fold
  init_sheet = sort paper
  q1 = [pt 0 0, pt 0.5 0, pt 0.5 0.5, pt 0 0.5]
  q_half = [pt 0 0, pt 0.5 0, pt 0.5 1, pt 0 1]

  in do
  it "first half has proper dst" $ do
    sort (solutionDestinations sol_half) `shouldBe` sort (q_half ++ [ pt 0 0, pt 0 1])
  it "has proper sources" $ do
    sort (solutionSources sol) `shouldBe` sort [pt 0 0, pt 0.5 0, pt 1 0, pt 1 0.5, pt 1 1, pt 0.5 1, pt 0 1, pt 0 0.5, pt 0.5 0.5]
  it "has correct facets" $ do
    faces_id_to_pt sol `shouldBe` sort' [ q1, map ( + pt 0.5 0) q1, map ( + pt 0.5 0.5) q1, map (+ pt 0 0.5) q1 ]
  it "has correct dsts" $ do
    sort (solutionDestinations sol) `shouldBe` sort (replicate 4 (pt 0 0) ++ replicate 2 (pt 0 0.5) ++ replicate 2 (pt 0.5 0) ++ [pt 0.5 0.5] )

tsk_random_lines :: [Line] -> Huzita ()
tsk_random_lines [] = return ()
tsk_random_lines (l:ls) = fold l >> tsk_random_lines ls

spec_destination_count :: (Huzita() -> Solution) -> Spec
spec_destination_count solver = describe "Destinations count" $ do
  it "should be equal to source count" $ QC.property $
    \ls -> let sol = solver $ tsk_random_lines ls
           in length (solutionDestinations sol) == length (solutionSources sol)

task1 :: Huzita ()
task1 = do
  let
    p1 = pt 1 0
    p2 = pt 0 1
    p = pt 1 1
  fold (Line p1 p2)
  let p5 = pt 0.5 0.5
      p6 = pt 0 0.5
  fold (Line p5 p6)
  end

task2' :: Huzita ()
task2' = do
  fold $ Line (pt 1 $ 1%6) (pt (1%6) 1)
  fold $ Line (pt (5%6) 1) (pt 0 $ 1%6)
  end

task2 :: Huzita ()
task2 = do
  fold $ Line (pt 1 $ 1%6) (pt (1%6) 1)
  fold $ Line (pt (5%6) 1) (pt 0 $ 1%6)
  fold $ Line (pt 0 $ 1%3) (pt 1 $ 1%3)
  end

tst_huzita_task1 :: (Huzita () -> Solution) -> Spec
tst_huzita_task1 solver = describe "Origami from Task" $ let
    sol = solver task1
    p a b c d = Point (Coord $ a%b) (Coord $ c%d)
    trueSources = [ pt 0 0, pt 1 0, pt 1 1, pt 0.5 1, pt 0 1, pt 0 0.5, pt 0.5 0.5 ]
    in do
      it "should give correct sources" $ do
        sort (solutionSources sol) `shouldBe`
          sort trueSources
      it "should give correct facets" $ do
        sort' [map (solutionSources sol !!) facet | facet <- solutionFacets sol] `shouldBe`
          sort' (map (map (trueSources !!)) [[0,1,6,5], [1,2,3,6], [3,4,6], [4,5,6]])
      it "should give correct destinations" $ do
        sort (solutionDestinations sol) `shouldBe`
          sort (replicate 3 (pt 0 0) ++ replicate 2 (pt 0 0.5) ++ [pt 0.5 0.5, pt 1 0])

tst_huzita_task2' :: (Huzita () -> Solution) -> Spec
tst_huzita_task2' solver = describe "Simplified Task2" $ let
    sol = solver task2'
    trueSources = [ pt (1%2) (2%3)
                  , pt 0 (1%6)
                  , pt 0 0
                  , pt 1 0
                  , pt 1 (1%6)
                  , pt (1%6) 1
                  , pt 0 1
                  , pt (5%6) 1
                  , pt 1 1 ]
    trueDests = [ pt (1%2) (2%3), pt 0 (1%6), pt 0 0
                , pt 1 0, pt 1 (1%6), pt (5%6) (1%3)
                , pt (5%6) (1%6), pt (1%6) (1%3), pt (1%6) (1%6)
                ]
    in do
      it "should give correct sources" $ do
        sort (solutionSources sol) `shouldBe`
          sort trueSources
      it "should give correct facets" $ do
        sort' [map (solutionSources sol !!) facet | facet <- solutionFacets sol] `shouldBe`
          sort' (map (map (trueSources !!)) [ [0,1,2,3,4], [0,4,8,7], [0,1,6,5], [0,5,7]
                                            ])
      it "should give correct destinations" $ do
        sort (solutionDestinations sol) `shouldBe`
          sort (map (trueDests !!) [0, 1, 2, 3, 4, 5, 6, 7, 8])

tst_huzita_task2 :: (Huzita () -> Solution) -> Spec
tst_huzita_task2 solver = describe "Origami similar to Ship" $ let
    sol = solver task2
    trueSources = [ pt 0 1, pt (1%6) 1, pt (5%6) 1, pt 1 1
                  , pt (1%2) (2%3)
                  , pt (1%6) (1%3), pt (5%6) (1%3)
                  , pt 0 (1%6), pt 1 (1%6)
                  , pt 0 0, pt 1 0 ]
    trueDests = [ pt 0 (2%3), pt (1%2) (2%3), pt 1 (2%3)
                , pt 0 (1%2), pt (1%6) (1%2), pt (5%6) (1%2), pt 1 (1%2)
                , pt (1%6) (1%3), pt (5%6) (1%3) ]
    in do
      it "should give correct sources" $ do
        sort (solutionSources sol) `shouldBe`
          sort trueSources
      it "should give correct facets" $ do
        sort' [map (solutionSources sol !!) facet | facet <- solutionFacets sol] `shouldBe`
          sort' (map (map (trueSources !!)) [ [0,1,5,7]
                                            , [1,2,4], [1,4,5]
                                            , [2,3,8,6], [2,4,6]
                                            , [4,5,6]
                                            , [5,6,8,10,9,7] ])
      it "should give correct destinations" $ do
        sort (solutionDestinations sol) `shouldBe`
          sort (map (trueDests !!) [5, 8, 7, 4, 1, 7, 8, 3, 6, 0, 2])



tst_random_huzita :: [Huzita () -> Solution] -> Spec
tst_random_huzita solvers = describe "Compare random tests" $ do
  it "Interpreters should agree" $ QC.property $
    \xps -> let mkLine (x,p) = Line (pt x 0) p
                solution : solutions = map ($ tsk_random_lines (map mkLine xps)) solvers
            in all (==solution) solutions

tst_transform :: (Huzita () -> Solution) -> Spec
tst_transform solver = describe "Transform" $ do
  let image task p = let sol = solver task
                         pointPairs = zip (solutionSources sol) (solutionDestinations sol)
                     in lookup p pointPairs
  it "Rotates (0,1) to (3%5,4%5)" $ do
    let task = transform (pt 3 4) (pt 0 0)
    image task (pt 0 1) `shouldBe` Just (pt 0.6 0.8)
  it "Moves (0,0) to (1,2)" $ do
    let task = transform (pt 0 1) (pt 1 2)
    image task (pt 0 0) `shouldBe` Just (pt 1 2)
  it "Rotates, then moves" $ do
    let task = transform (pt 3 4) (pt 1 2)
    image task (pt 0 1) `shouldBe` Just (pt 1.6 2.8)

huzitaTests :: (Huzita () -> Solution) -> Spec
huzitaTests solver = do
--  spec_destination_count solver
  tst_transform solver
  tst_huzita_void solver
  tst_single_fold solver
  tst_single_fold2 solver
  tst_single_fold3 solver
  tst_single_fold4 solver
  spec_double_fold solver
  tst_huzita_task1 solver
  tst_huzita_task2' solver
  tst_huzita_task2 solver


multiHuzitaTest :: Spec
multiHuzitaTest = do
  helpers_tests
  describe "Huzita1" $ huzitaTests solveHuzita1
  describe "Huzita2" $ huzitaTests HI2.solveHuzita2
  describe "Huzita1 == Huzita2" $ tst_random_huzita [solveHuzita1, HI2.solveHuzita2]

