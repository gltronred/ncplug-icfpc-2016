{-# LANGUAGE ViewPatterns #-}

module PaperFold1Tests where

import Control.Arrow ((***))
import Data.List (sort)
import Data.Ratio

import Test.Tasty.Hspec
import qualified Test.Tasty.QuickCheck as QC

import PaperFold1


equalIgnoringSource :: MPoint -> MPoint -> Bool
equalIgnoringSource (finalXY -> p1) (finalXY -> p2) = p1 == p2

{-
paperFold1Tests :: Spec
paperFold1Tests = describe "Tests for paper folds" $ do
  it "foldInHalfV should really do it" $ do
    let halfs@(p1:p2:_) = foldInHalfV [initialPaper]
    length halfs `shouldBe` 2
    all (uncurry equalIgnoringSource) (zip (sort $ paper_vertices p1) (sort $ paper_vertices p2)) `shouldBe` True

  it "foldInHalfH should really do it" $ do
    let halfs@(p1:p2:_) = foldInHalfH [initialPaper]
    length halfs `shouldBe` 2
    all (uncurry equalIgnoringSource) (zip (sort $ paper_vertices p1) (sort $ paper_vertices p2)) `shouldBe` True
-}
