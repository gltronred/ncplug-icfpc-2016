{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module HuzitaInterpreter1 where

import Test.Tasty.Hspec
import qualified Test.Tasty.QuickCheck as QC

import Types
import Huzita
import Huzita.Interpreter2

import Data.List (sort)
import Data.Ratio

-- deriving instance QC.Arbitrary Coord
-- instance QC.Arbitrary Point where
--   arbitrary = Point <$> QC.arbitrary <*> QC.arbitrary
-- instance QC.Arbitrary Line where
--   arbitrary = Line <$> QC.arbitrary <*> QC.arbitrary

-- task1 :: Huzita ()
-- task1 = do
--   let p1 = pt 1 0
--       p2 = pt 0 1
--       p = pt 1 1
--   fold (Line p1 p2) p
--   let p5 = pt 0.5 0.5
--       p6 = pt 0 0.5
--   fold (Line p5 p6) p1
--   end

task1 :: Huzita ()
task1 =
  let
    p1 = pt 1 0
    p2 = pt 0 1
    l1 = Line p1 p2

    p3 = pt 0 0.5
    p4 = pt 1 0.5
    l2 = Line p3 p4

  in do

  fold l1 (pt 1 1)
  fold l2 (pt 1 1)

