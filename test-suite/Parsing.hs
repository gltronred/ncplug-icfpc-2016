module Parsing where

import Test.Tasty.Hspec
import Types

import Data.Ratio
import qualified Data.Vector as V
import Text.Parsec

problem3_in = "1\n4\n\n-1/2,-1/2\n1/2,-1/2\n1/2,1/2\n-1/2,1/2\n4\n-1/2,-1/2 1/2,-1/2\n-1/2,-1/2 -1/2,1/2\n1/2,-1/2 1/2,1/2\n-1/2,1/2 1/2,1/2"

problem3_out =
  Problem {
    problemSilhouette = Silhouette {
       polygons = V.fromList
                  [ Polygon False $ V.fromList
                    [ Point (Coord $ -1 % 2) (Coord $ -1 % 2)
                    , Point (Coord $  1 % 2) (Coord $ -1 % 2)
                    , Point (Coord $  1 % 2) (Coord $  1 % 2)
                    , Point (Coord $ -1 % 2) (Coord $  1 % 2)
                    ]
                  ]
       },
    problemSkeleton = Skeleton {
      segments = V.fromList
                 [ Line (Point (Coord $ -1 % 2) (Coord $ -1 % 2)) (Point (Coord $  1 % 2) (Coord $ -1 % 2))
                 , Line (Point (Coord $ -1 % 2) (Coord $ -1 % 2)) (Point (Coord $ -1 % 2) (Coord $  1 % 2))
                 , Line (Point (Coord $  1 % 2) (Coord $ -1 % 2)) (Point (Coord $  1 % 2) (Coord $  1 % 2))
                 , Line (Point (Coord $ -1 % 2) (Coord $  1 % 2)) (Point (Coord $  1 % 2) (Coord $  1 % 2))
                 ]
      }
    }

cwRect = [ Point (Coord 0) (Coord 0)
         , Point (Coord 0) (Coord 1)
         , Point (Coord 1) (Coord 1)
         , Point (Coord 1) (Coord 0)
         ]

ccwRect = [ Point (Coord 0) (Coord 0)
          , Point (Coord 1) (Coord 0)
          , Point (Coord 1) (Coord 1)
          , Point (Coord 0) (Coord 1)
          ]

parserTests :: Spec
parserTests = describe "Parsing tests" $ do
  it "should parse 1/2 and -1/2" $ do
    parse coordParser "" "1/2" `shouldBe` Right (Coord $ 1 % 2)
    parse coordParser "" "-1/2" `shouldBe` Right (Coord $ -1 % 2)
  it "should parse point 1/2,-1/2" $ do
    parse pointParser "" "1/2,-1/2" `shouldBe` Right (Point (Coord $ 1 % 2) (Coord $ -1 % 2))
  it "should determine cw" $ isClockwise cwRect `shouldBe` True
  it "should determine ccw" $ isClockwise ccwRect `shouldBe` False
  it "should parse Problem 3 (A)" $ do
    parse problemParser "" problem3_in `shouldBe` Right problem3_out

