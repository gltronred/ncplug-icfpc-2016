{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module HuzitaAxioms where

import Test.Tasty.Hspec
import qualified Test.Tasty.QuickCheck as QC

import Types
import Huzita
import qualified Huzita.Interpreter0 as HI0

import Data.List (find)
import Data.Maybe (isJust)

deriving instance QC.Arbitrary Coord
instance QC.Arbitrary Point where
  arbitrary = Point <$> QC.arbitrary <*> QC.arbitrary
instance QC.Arbitrary Line where
  arbitrary = Line <$> QC.arbitrary <*> QC.arbitrary

huzitaAxiomsTests :: Spec
huzitaAxiomsTests = describe "Tests for Huzita Axioms" $ do
  describe "Axiom 1. Given two points, there is unique fold passing through both of them" $ do
    it "should pass through both points" $ QC.property $
      \p1 p2 -> p1 /= p2 QC.==>
                isJust (find (==Line p1 p2) $ HI0.folds (HI0.runHuzitaPure (axiom1 p1 p2) HI0.initial))
  -- describe "Axiom 2. Given two points p1 and p2, there is a unique fold that places p1 onto p2" $ do
  --   it "should 

