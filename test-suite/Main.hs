-- Tasty makes it easy to test your code. It is a test framework that can
-- combine many different types of tests into one suite. See its website for
-- help: <http://documentup.com/feuerbach/tasty>.
import qualified Test.Tasty
-- Hspec is one of the providers for Tasty. It provides a nice syntax for
-- writing tests. Its website has more info: <https://hspec.github.io>.
import Test.Tasty.Hspec
import qualified Test.Hspec.QuickCheck as QC
import qualified Test.Hspec.SmallCheck as SC
import Test.Tasty.QuickCheck as QC
import Test.Tasty.SmallCheck as SC

import Parsing
import HelperTests
-- import PaperFold1Tests
import HuzitaAxioms

-- tests
import HuzitaInterpreter1
import SolverTests

main :: IO ()
main = do
    test <- testSpec "ncplug-icfpc2016" spec
    Test.Tasty.defaultMain test

spec :: Spec
spec = parallel $ do
  parserTests
  helpersTests
  -- paperFold1Tests

  huzitaAxiomsTests
  multiHuzitaTest
  solverTests

  -- describe "unit tests" $
  --   it "is trivially true" $ do
  --     True `shouldBe` True

  -- describe "smallcheck" $
  --   it "Fermat's last theorem" $ SC.property $
  --     \x y z n ->
  --       (n :: Integer) >= 3 SC.==> x^n + y^n /= (z^n :: Integer)

  -- describe "quickcheck" $
  --   it "Fermat's last theorem" $ QC.property $
  --     \x y z n ->
  --       (n :: Integer) >= 3 QC.==> x^n + y^n /= (z^n :: Integer)
