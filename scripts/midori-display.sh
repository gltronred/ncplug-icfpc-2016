#!/bin/sh

cat >./reload.html <<EOF
<head>
<script>
  function reloadCaptcha(sImgElementId) {
      var oImgElement = document.getElementById(sImgElementId);
      oImgElement.setAttribute('src', "./out.svg" + '?' + Math.random());
      return false;
  }

 setInterval(function() {
    reloadCaptcha ('nlc')
 }, 300);

</script>
</head>
<body>
  <img id="nlc" name="nlc" src="./out.svg"
    onClick="javascript:reloadCaptcha('nlc'); return false;" />
</body>
EOF

midori --plain ./reload.html >/dev/null 2>&1 &

PID=$!

while [ "$WID" == "" ]; do
    WID=$(wmctrl -lp | grep $PID | cut "-d " -f1)
done

echo WID found: $WID

wmctrl -r $WID -b remove,fullscreen
wmctrl -v -r $WID -b add,above
wmctrl -v -r $WID -b add,sticky
wmctrl -i -r $WID -e 0,350,50,650,600
