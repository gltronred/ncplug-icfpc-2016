#!/bin/bash

DT=$1
F=$2

TS=$(date -d"$DT" +%s)
echo Publishing problem $F at $DT = $TS s
curl --compressed -L -H Expect: -H 'X-API-Key: 117-4b057fd853b2b47591f1c1891730df9b' -F "solution_spec=@$F" -F "publish_time=$TS" 'http://2016sv.icfpcontest.org/api/problem/submit'

