#!/bin/bash

if [[ $# == 0 ]]
then
    echo "Usage: solve-problems.sh <solver> <problemId> [<problemId>...]"
fi

SOLVER=$1

[[ -d problems/solved ]] || mkdir problems/solved

while [[ $# > 1 ]]
do
    shift 1
    ID=$1
    if [[ -f problems/$ID.json ]]
    then 
	# echo $SOLVER solves $ID
	timeout 10s stack exec $SOLVER problems/$ID.json > /tmp/$SOLVER-$ID.txt
	if [[ $(bc <<< "$(head -n1 /tmp/$SOLVER-$ID.txt) <= 0") == 1 ]]
	then
	    echo $ID: PE: sources should be greater than 0
	else 
	    CONTENT=$(curl --compressed --silent -L -H Expect: -H 'X-API-Key: 117-4b057fd853b2b47591f1c1891730df9b' -F "problem_id=$ID" -F "solution_spec=@/tmp/$SOLVER-$ID.txt" 'http://2016sv.icfpcontest.org/api/solution/submit')
	    # echo $CONTENT
	    if [ $(echo $CONTENT | jq '.ok') == "true" ]
	    then
		RESEMBLANCE=$(echo $CONTENT | jq '.resemblance')
		if (( $(bc <<< "$RESEMBLANCE == 1") ))
		then
		    echo $ID: AC
		    mv problems/$ID.json problems/solved/
		    mv /tmp/$SOLVER-$ID.txt problems/solved/$ID-$SOLVER.txt
		else if (( $(bc <<< "$RESEMBLANCE >= 0") ))
		     then
			 echo $ID: WA: $RESEMBLANCE
		     fi
		fi
	    else
		echo $ID: PE: $CONTENT
	    fi
	    sleep 1
	fi
    else
	echo $ID seems to be solved
    fi
done

