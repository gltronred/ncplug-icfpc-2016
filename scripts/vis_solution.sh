#!/bin/bash

set -e

TMP=/tmp/single.svg

SOLVER=$1
PROBLEM=$2

stack exec $SOLVER $PROBLEM > sol.txt && stack exec visualizer solution  sol.txt  sol.svg && display sol.svg
