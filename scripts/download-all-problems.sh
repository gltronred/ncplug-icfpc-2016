#!/bin/bash

TBLOB=$(curl --compressed -L -H Expect: -H 'X-API-Key: 117-4b057fd853b2b47591f1c1891730df9b' 'http://2016sv.icfpcontest.org/api/snapshot/list' | jq -r '.snapshots[-1:]|.[].snapshot_hash')
echo $TBLOB
sleep 1
curl --compressed -L -H Expect: -H 'X-API-Key: 117-4b057fd853b2b47591f1c1891730df9b' "http://2016sv.icfpcontest.org/api/blob/$TBLOB" | tee log.log | jq -r '.problems | map([.problem_id, .problem_spec_hash] | @csv)' | tr -d '[]"\\' | sed 's/ *//g; /^$/d; s/,/ /g;' | while read line
do
    x=($line)
    if [[ ! -f problems/${x[0]}.json && ! -f problems/solved/${x[0]}.json ]]
    then
	sleep 1
	echo "Downloading ${x[0]}... "
	curl --silent --compressed -L -H Expect: -H 'X-API-Key: 117-4b057fd853b2b47591f1c1891730df9b' "http://2016sv.icfpcontest.org/api/blob/${x[1]}" > problems/${x[0]}.json
    else
	echo "Problem ${x[0]} already exists"
    fi
done
